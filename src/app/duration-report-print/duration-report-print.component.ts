import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LotteryStorageService } from '../services/lottery-storage.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-duration-report-print',
  templateUrl: './duration-report-print.component.html',
  styleUrls: ['./duration-report-print.component.scss']
})
export class DurationReportPrintComponent implements OnInit {
  ReportDetails: any[]=[];
  billModel1: any = [];
  userD = this.storageService.get('currentUser');
  firstName = this.userD.user.firstName;
  lastName = this.userD.user.lastName;
  retailerID = this.userD.user.userCode;
  claimAmount:any []=[];
  date = Date();
  today= this.datePipe.transform(this.date, "dd-MM-yyyy");

  
  constructor( private datePipe: DatePipe, private route: Router, private storageService: LotteryStorageService) { }

  ngOnInit() {
    this.ReportDetails = this.storageService.get('durationreportDetails').DateWise;
    console.log('bill model', this.ReportDetails);
    this.claimAmount = this.ReportDetails;
      // this.currentBalance = this.ReportDetails.currentBalance,
      // this.payToCompany = this.ReportDetails.payToCompany,
      // this.ticketBooked = this.ReportDetails.ticketBooked 
      // this.cashInDrawer = this.ReportDetails.cashAmount
      // this.profit = this.ReportDetails.profit,
      // this.totalWalletRecharge = this.ReportDetails.totalWalletRecharge,
      // this.reportDate = this.ReportDetails.reportDate,
      // this.name = this.ReportDetails.name,
      // this.dailyRecharge = this.ReportDetails.dailyRecharge,
      // this.closingBalance = this.ReportDetails.closingBalance,
      // this.openingBalance = this.ReportDetails.openingBalance
      setTimeout(() => {
        console.log('Test');
        window.print();
        this.back();
    }, 1000/60);  
  }
  back() {
    this.route.navigate(['/client/daily-report']);

  }

}
