import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DurationReportPrintComponent } from './duration-report-print.component';

describe('DurationReportPrintComponent', () => {
  let component: DurationReportPrintComponent;
  let fixture: ComponentFixture<DurationReportPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DurationReportPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DurationReportPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
