import { Component, OnInit } from '@angular/core';
import { LotteryStorageService } from '../../services/lottery-storage.service';
import { LotteryHttpService } from '../../services/lottery-http.service';
import { createCipher } from 'crypto';
import { Router } from '@angular/router';
import {version} from '../../../../package.json';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loggedInData = {
    'firstName': 'Ayush',
    'lastName': 'Saxena'
  };
  baseImageUrl = '/assets/images/cris.jpg';
  imageUrl: '/assets/images/cris.jpg';
  userD = this.storageService.get('currentUser');
  firstName = this.userD.user.firstName;
  lastName = this.userD.user.lastName;
  phoneNumber =  this.userD.user.phoneNumber;
phoneDisp = "("+ this.phoneNumber +")";
  role = this.userD.user.roleMaster.name;
  retailerID = this.userD.user.userCode;
  today: number = Date.now();
  appVersion:any =' '+ version;
  constructor( private storageService: LotteryStorageService, private lotteryHttpService: LotteryHttpService, private router:Router) {
    setInterval(() => {
      this.today = Date.now()
    }, 1000);
  }

  bookingslot = [
  ]
  match :any;
  matchtime : any;
matchEndtime: any;
drawTimeAm : any;
leftslot =1;
showamt=false;
disableprint = false;
start : any;
draw : any;
  checktime(){
    //this.leftslot= this.countslot;
    this.today;
    let h :any;
    let m :any;
    var d = new Date();
   h = d.getHours();
   m = d.getMinutes()
  //  console.log('time');
  //  console.log(h);
  //  console.log(m);
  let min;
  if(m<10){
    min= "0"+m;
  }else{
min =m
  }
   this.match= h+":"+min;
   let time1="14:00";
   let time2 ="15:00"

  //  this.match= "19:00";
   for(let book of this.bookingslot)
   {
     if(this.match>=book.startTime && this.match<=book.endTime)
     {
              this.matchtime=book.startTime;
              this.matchEndtime=book.drawTime;  
              
              if(this.match >= time1 && this.match<=time2){
                this.drawTimeAm="Lunchtime"
              }else{
                this.drawTimeAm= book.drawTimeAm;
              }
     }
   }
// this.start = new Date();
// {{ this.start | this.matchEndtime}}
// this.draw= this.start.getTime();
    }
  getslotlist(){
    this.lotteryHttpService.makeRequestApi('get', 'slotlist').subscribe(slot => {
      if (slot !=null) {
      this.bookingslot= slot;
      console.log('booking slot ali header------', this.bookingslot);
    //  this.checktime();
      }
      else{
        console.log('error');
      }
  
    });
  }

  coun : any;
  diff : any;
  splt(string : any, nb : any) {
    var array = string.split(':');
    return array[nb];
}

splt1(string : any, nb : any) {
  var array = string.split(':');
  return array[nb];
}
  mint =14;
  sec = 60;
  tm : any;
 tmt = this.mint + ":"+ this.sec; 
 second : any;
 minutes : any;
  countdowntimer(){
    var d = new Date();
    let h :any;
      let m :any;
      let s: any;
    h = d.getHours();
    m = d.getMinutes();
    s = d.getSeconds();
    let min;
    if(m<10){
      min= "0"+m;
    }else{
  min =m
    }
  
   this.tm = min + ":"+ s;

   for(let book of this.bookingslot)
   {
     if(this.match>=book.startTime && this.match<=book.endTime)
     {
              this.matchtime=book.startTime;
              this.matchEndtime=book.drawTime;  
            //  this.drawTimeAm= book.drawTimeAm;
     }
   }

  
 let check = this.splt(this.matchtime, 1);
  let check1 = this.splt1(this.matchEndtime, 1);
  //console.log('check1',check1);
  let mint;
  let secs;
  if(check1==0){
    // this.minutes= 59-m;
    mint= 59-m;
    if(mint<10){
      this.minutes= "0"+mint;
    }else{
      this.minutes=mint;
    }
    secs= this.sec-s;
    if(secs<10){
      this.second= "0"+secs;
    }else{
      this.second=secs;
    }

  }else if(check1-m > 15){
    mint= (--check1)-(m+check);
    if(mint<10){
      this.minutes= "0"+mint;
    }else{
      this.minutes=mint;
    }
    secs= this.sec-s;
    if(secs<10){
      this.second= "0"+secs;
    }else{
      this.second=secs;
    }
  }
  else{
    mint= (--check1)-m;
    if(mint<10){
      this.minutes= "0"+mint;
    }else{
      this.minutes=mint;
    }
    secs= this.sec-s;
    if(secs<10){
      this.second= "0"+secs;
    }else{
      this.second=secs;
    }
  }

// if(this.mint==m){

// }else{
//   this.mint= this.mint-m; 
// }
//    this.minutes= this.mint;
//    this.sec=this.sec-1;
//    this.second= this.sec;
//    if(this.sec==0)
//    {
//     this.mint= this.mint-1;
//     this.sec=60; 
//    }

  
    
  }

  toTime(timeString){
    var timeTokens = timeString.split(':');
    return new Date(1970,0,1, timeTokens[0], timeTokens[1]);
}

  ngOnInit() {
     this.getslotlist();
    // setInterval(() => {
    //   this.getslotlist(); 
    //   }, 1000);
    setInterval(() => {
      this.checktime(); 
      }, 1000);

      setInterval(() => {
        this.countdowntimer(); 
        }, 1000);
     
  }
  

  navigateTo(value: string) {
    if (value == 'a') {
      console.log('Profile',value)
        this.router.navigate(['/client/user-profile']);
    }
    else if (value == 'b') {
      console.log('setting',value)
       this.router.navigate(['/client/setting']);
  }
  else if (value == 'c') {
    console.log('logout',value)
   this.router.navigate(['/login']);
}
    
}
refresh(): void {
  window.location.reload();
}

}
