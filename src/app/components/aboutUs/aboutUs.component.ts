import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { Router, ActivatedRoute } from '@angular/router';
import { LotteryStorageService } from '../../services/lottery-storage.service';
import { SweetAlertService } from '../../common/sharaed/sweetalert2.service';
import { LotteryHttpService } from '../../services/lottery-http.service';
import * as $ from '../../../../node_modules/jquery'
import { version } from '../../../../package.json';
@Component({
selector: 'app-aboutUs',
templateUrl: './aboutUs.component.html',
styleUrls: ['./aboutUs.component.scss']
})
export class AboutUsComponent implements OnInit {
public clientDetailsForm: FormGroup
public bankDetailsForm: FormGroup
public kycDetailsForm: FormGroup
public uploader: FileUploader = new FileUploader({});
imageUrl: string | ArrayBuffer;
imagePK: any;
userD = this.storageService.get('currentUser');
userName = this.userD.user.userName;
phoneNumber = this.userD.user.phoneNumber;
phoneDisp = "(" + this.phoneNumber + ")";
role = this.userD.user.roleMaster.name;
isEdit = false;
loggedInUserDetails = this.storageService.get('currentUser');
userid = this.loggedInUserDetails.user.id;
constructor(private storageService: LotteryStorageService, private alertService: SweetAlertService,
private router: Router, public route: ActivatedRoute, private fb: FormBuilder,
private lotteryService: LotteryHttpService) {
}
appVersion: any = ' ' + version;

ngOnInit() {


}



}