import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-slots-reports',
  templateUrl: './slots-reports.component.html',
  styleUrls: ['./slots-reports.component.scss']
})
export class SlotsReportsComponent implements OnInit {
  SlotsDetails:any[]=[];
  slotTime:any;
  TodayDate:any;
  viewSlotsDetails = this.storageService.get('slotsData');
  viewSlotsId = this.storageService.get('id');
  viewSlotsdrawTime = this.storageService.get('drawTime');
  constructor(private fb: FormBuilder, private storageService: LotteryStorageService,
    private route: Router, private alertService: SweetAlertService,private datePipe: DatePipe,
   private lotteryService: LotteryHttpService) { }

  ngOnInit() {

    this.SlotsDetails = this.viewSlotsDetails.bookingDetail
    console.log('National',this.SlotsDetails);
    this.slotTime = this.viewSlotsdrawTime

  }
  back(){
    this.route.navigate(['/distributor/distributor-report']);
      }
}
