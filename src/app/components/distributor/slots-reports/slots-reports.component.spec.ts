import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlotsReportsComponent } from './slots-reports.component';

describe('SlotsReportsComponent', () => {
  let component: SlotsReportsComponent;
  let fixture: ComponentFixture<SlotsReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlotsReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlotsReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
