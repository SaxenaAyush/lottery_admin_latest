import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LotteryStorageService } from "../../../services/lottery-storage.service";
import { ActivatedRoute, Router } from "@angular/router";
import { SweetAlertService } from "../../../common/sharaed/sweetalert2.service";
import { LotteryHttpService } from "../../../services/lottery-http.service";

@Component({
  selector: 'app-add-new-client',
  templateUrl: './add-new-client.component.html',
  styleUrls: ['./add-new-client.component.scss']
})
export class AddNewClientComponent implements OnInit {112233
  public addClientForm: FormGroup
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  id= this.userD.user.roleMaster.id;
  phoneNumber =  this.userD.user.phoneNumber;
  phoneDisp = "("+ this.phoneNumber +")";
  role = this.userD.user.roleMaster.name;
  isDisabled = false;
  roleList = [
    { role: "Admin", value: 1 },
    { role: "Distributor", value: 2 },
    { role: "Sub-Distributor", value: 3 },
    { role: "Retailer", value: 4 }
  ];
  roleList2 = [
    
    { role: "Sub-Distributor", value: 3 },
    { role: "Retailer", value: 4 }
  ];
  roleList3 = [
    
    
    { role: "Retailer", value: 4 }
  ];
  constructor(private storageService: LotteryStorageService, private alertService: SweetAlertService,
    private router: Router, public route: ActivatedRoute, private fb: FormBuilder,
    private lotteryService: LotteryHttpService) {
  }

  ngOnInit() {
    // this.getRole();
    this.addClientForm = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required]],
      role: ['', Validators.required],
      shopName: ['', [Validators.required]],
      shopAddress: ['', [Validators.required]],
      city: ['', [Validators.required]],
      state: ['', Validators.required],
      pinCode: ['', Validators.required],
    });
  }
  roleModel: any = {
    "id": 0
  }

  onChange(event){
    console.log('event', event);
    console.log('roleModel',this.roleModel.id);
  }
  roleid(id: any) {
    this.roleModel.id = id;
    console.log('id', this.roleModel.id);
  }
  addClient() {
    this.isDisabled=true;
      let reqMap = {
        ...this.addClientForm.value,
        createdBy: this.storageService.get('currentUser').user.id,
        statusMaster: {
          id: 1
        },
        roleMaster:{
          id: this.roleModel.id
        }
      }
          console.log(reqMap);
         
      this.lotteryService.makeRequestApi('post', 'addClient', reqMap
      ).subscribe((res) => {
        console.log(res);
        if(res.code == 207){
          this.alertService.swalError(res.message);
          this.isDisabled=false;

        } if(res.code == 206){
          this.alertService.swalSuccess('Client Successfully Added');
          this.roleModel.id = 0;
          this.router.navigate(['/distributor/view-client']);
          this.isDisabled=false;
        }
        
        
      }, err => {
        console.log(err);
      });
   
   



  }

  bookingslot: any;
  // getslotlist(){
  //   this.lotteryService.makeRequestApi('get', 'getRoleList').subscribe(slot => {
  //     if (slot !=null) {
  //     this.bookingslot= slot;
  //     // this.getcurrentSlot();
  //     }
  //     else{
  //       console.log('error');
  //     }

  //   });
  // }


  // getRole() {

  //   this.lotteryService.makeRequestApi('get', 'getRoleList',
  //     {}).subscribe(res => {
  //     if (!res.isError) {
  //       this.rolelist = res;
  //       for (let role of this.rolelist) {
  //         this.role.push({
  //           "name": role.name
  //         });
  //       }
  //     }
  //   });
  // }
}
