import { Component, OnInit } from '@angular/core';
import {FileUploader} from 'ng2-file-upload';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { LotteryStorageService } from '../../../services/lottery-storage.service';

@Component({
  selector: 'app-advertisement',
  templateUrl: './advertisement.component.html',
  styleUrls: ['./advertisement.component.scss']
})
export class AdvertisementComponent implements OnInit {
  public uploader: FileUploader = new FileUploader({});
  imageUrl: string | ArrayBuffer;
  imagePK:any;
  loggedInUserDetails: any;
  constructor(private lotteryService: LotteryHttpService, private storageService:LotteryStorageService) { 
    this.uploader.onAfterAddingFile = (file) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        this.imageUrl = reader.result;
        const imgData = {
          'detail': reader.result,
          'entityName': 'EOMenuItem',
          'type': file.file.type
        };
       console.log('image',imgData);
       this.storageService.set('imageData', imgData);
        // this.lotteryService.makeRequestApi('post', 'createImgObject', imgData).subscribe((res: any) => {
        //   this.imagePK = res.data.primaryKey;
        // }, err => {
        // });
      };
      console.log(reader.readAsDataURL(file._file));
    };
  }

  ngOnInit() {
    this.loggedInUserDetails = this.storageService.get('imageData');
    if (this.loggedInUserDetails.eoImage !== null) {
      this.imageUrl = this.loggedInUserDetails.eoImage.imageUrl;
    }
  }

}
