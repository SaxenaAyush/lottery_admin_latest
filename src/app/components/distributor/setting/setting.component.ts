import { Component, OnInit } from '@angular/core';
import { LotteryStorageService } from "../../../services/lottery-storage.service";
import { ActivatedRoute, Router } from "@angular/router";
import { SweetAlertService } from "../../../common/sharaed/sweetalert2.service";
import { LotteryHttpService } from "../../../services/lottery-http.service";
import * as $ from '../../../../../node_modules/jquery';
@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  userD = this.storageService.get('currentUser');
  userID = this.storageService.get('currentUser').user.id;
    id= this.userD.user.roleMaster.id;
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  commissionWalletList: any = [];
  commissionClaimList: any = [];
  requestedWalletList: any = [];
  requestedClaimList: any = [];
  walletId: any;
  claimId: any;
  commission: any = [];
  today: number = Date.now();
  role = this.userD.user.roleMaster.name;
  constructor(private storageService: LotteryStorageService, private alertService: SweetAlertService,
    private router: Router, public route: ActivatedRoute,
    private lotteryService: LotteryHttpService) { }

  passwordModel: any = {
    "securityStamp": "",
    "passwordHash": "",
    "confirm": "",

  }
  navModel: any = {
    "claim": 1,
    "setting": 2,

  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.setting = 2;
      this.navModel.claim = 1;
    }
    else if (input == 2) {
      this.navModel.setting = 1;
      this.navModel.claim = 2;
    }
  }
  newModel: any = {
    "claimBouns": 2,
    "walletBonus": 1,

  }
  newbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.newModel.walletBonus = 2;
      this.newModel.claimBouns = 1;
    }
    else if (input == 2) {
      this.newModel.walletBonus = 1;
      this.newModel.claimBouns = 2;
    }
  }
  ngOnInit() {
    console.log('humaara panchar  ',this.userID);
  }
  changePwd() {

    if (this.passwordModel.passwordHash == "") {
      this.alertService.swalError('Enter the value');
      this.passwordModel.passwordHash = "";
      this.passwordModel.confirm = "";
      this.passwordModel.securityStamp = "";
      this.passwordModel.id = this.userID;
    }
    else if (this.passwordModel.passwordHash == this.passwordModel.confirm) {
      this.lotteryService.makeRequestApi('post', 'changePassword', {
        
        securityStamp: this.passwordModel.securityStamp,
        passwordHash: this.passwordModel.passwordHash,
        id: this.userID
      }).subscribe(
        res => {
          console.log('change pwd', res);
          if (res.code == 209) {
            this.alertService.swalError('Old Pass code is not match');
            this.passwordModel.passwordHash = "";
            this.passwordModel.confirm = "";
            this.passwordModel.securityStamp = "";
          } else if (res.code == 206) {
            this.alertService.swalSuccess('New Password is saved ');
            this.passwordModel.passwordHash = "";
            this.passwordModel.confirm = "";
            this.passwordModel.securityStamp = "";
          }
        });
    } else {
      this.alertService.swalError('New Password and Re-Type password is not match');
      this.passwordModel.passwordHash = "";
      this.passwordModel.confirm = "";
      this.passwordModel.securityStamp = "";
    }



  }
  openPassword() {
    $('.claim-Form').hide();
    $('.password-Form').show();
    $('.newScreen-Form').hide();
  }
  openClaim() {
    $('.claim-Form').show();
    $('.password-Form').hide();
    $('.newScreen-Form').show();

    this.claimBonus();
  }
  requestedWallet: any = {
    "commisionAmount": "",
    "id": "",
  }
  walletBonus() {
    $('.claimBonus-Form').hide();
    $('.walletBonus-Form').show();
    let reqMap = {
      type: "wallet"
    }
    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'commission', reqMap).subscribe((res) => {
      this.commissionWalletList = res.content;

      // for (let item of this.commissionWalletList) {
      //   this.commissionWalletList.push(item);
      // }
      // console.log(this.commissionWalletList);

    });

  }
  walletList;
  ClaimList;
  requestedClaim: any = {
    "commisionAmount": "",
    "id": "",
  }
  claimBonus() {
    $('.claimBonus-Form').show();
    $('.walletBonus-Form').hide();
    let reqMap = {
      type: "claim"
    }
    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'commission', reqMap).subscribe((res) => {
      this.commissionClaimList = res.content;
      for (let i of this.commissionClaimList) {
        this.commission.push({
          "amount": i.commisionAmount,
        })
      }
      console.log(this.commission);
      // this.commission = this.commissionClaimList.commisionAmount;
      console.log('commsionClaimList', this.commissionClaimList);
    });
  }
  onChange1(event) {
    console.log('event', event);
    this.walletList = event.target.value;
  }
  saveWalletBonus(id, emp) {
    let reqMap = {
      type: "wallet",
      role: {
        id: id
      },
      id: emp,
      createBy: this.storageService.get('currentUser').user.id,
      commisionAmount: this.walletList,
    }

    this.lotteryService.makeRequestApi('post', 'saveCommission', reqMap).subscribe((res) => {
      const resObj = res;
      console.log(this.commissionClaimList);
      this.commissionWalletList.push(resObj);
      this.walletBonus();
    });
  }
  onChange(event) {
    console.log('event', event);
    this.ClaimList = event.target.value;
  }
  saveClaimBonus(id1, emp1) {
    let reqMap = {
      type: "claim",
      role: {
        id: id1
      },
      id: emp1,
      createBy: this.storageService.get('currentUser').user.id,
      commisionAmount: this.ClaimList
    }

    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'saveCommission', reqMap).subscribe((res) => {
      const resObj = res;
      console.log(this.commissionClaimList);
      this.commissionClaimList.push(resObj);
      this.claimBonus();
    });

  }
 
}
