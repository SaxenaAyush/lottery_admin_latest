import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { DataSharingService } from '../../../services/data-sharing.service';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {

  public userForm: FormGroup;
  loggedInUserDetails: any;
  clientDetailsForm: any;
  Details: any = [];
  userfirstname: any = [];
  userlastname: any = [];
  userphoneno: any = [];
  useremail: any = [];
  userDetails: any = [];
  kycdocName: any = [];
  kycdocNumber: any = [];
  bankName: any = [];
  accountNumber: any = [];
  ifscCode: any = [];
  branchname: any = [];
  address: any = [];
  city: any = [];
  pin: any = [];
  state: any = [];
  shopName: any = [];
  addresstype: any = [];
  isEdit = false;
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  public personalDetailsForm: FormGroup
  public bankDetailsForm: FormGroup
  public kycDetailsForm: FormGroup
  today: number = Date.now();
  constructor(private fb: FormBuilder, private lotteryService: LotteryHttpService, private storageService: LotteryStorageService,
    private httpClient: HttpClient, private dataSharingService: DataSharingService, private alertService: SweetAlertService) { }

  ngOnInit() {
    this.loggedInUserDetails = this.storageService.get('currentUser');
    this.initForm();
    console.log(this.loggedInUserDetails);
  
  }
  


  initForm() {
    if (this.loggedInUserDetails.user) {
      this.userForm = this.fb.group({
        firstName: [this.loggedInUserDetails.user.firstName],
        lastName: [this.loggedInUserDetails.user.lastName],
        homeAddress: this.initHomeAddress(),
        currentAddress: this.initCurrentAddress(),
        bandDetail: this.initBankDetails(),
        kycDetail: this.initKycDetails(),
        phoneNumber: [this.loggedInUserDetails.user.phoneNumber],
        email: [this.loggedInUserDetails.user.email],
      });
    }
  }

  initHomeAddress() {
    if (this.loggedInUserDetails.permanentDetail !== null) {
      return this.fb.group({
        shopName: ['', [Validators.required]],
        address: [this.loggedInUserDetails.permanentDetail.address],
        city: [this.loggedInUserDetails.permanentDetail.city],
        state: [this.loggedInUserDetails.permanentDetail.state],
        pin: [this.loggedInUserDetails.permanentDetail.pin],
        addressType: [this.loggedInUserDetails.permanentDetail.addressType],
        status: [0],
      });
    } else {
      return this.fb.group({
        shopName: [''],
        address: [''],
        city: [''],
        state: [''],
        pin: [''],
        addressType: [''],
        status: [1],
      });
    }
  }
  initCurrentAddress() {
    if (this.loggedInUserDetails.permanentDetail !== null) {
      return this.fb.group({
        shopName: ['', [Validators.required]],
        address: [this.loggedInUserDetails.permanentDetail.address],
        city: [this.loggedInUserDetails.permanentDetail.city],
        state: [this.loggedInUserDetails.permanentDetail.state],
        pin: [this.loggedInUserDetails.permanentDetail.pin],
        addressType: [this.loggedInUserDetails.permanentDetail.addressType],
        status: [0],
      });
    } else {
      return this.fb.group({
        shopName: [''],
        address: [''],
        city: [''],
        state: [''],
        pin: [''],
        addressType: [''],
        status: [1],
      });
    }
  }
  initBankDetails() {
    if (this.loggedInUserDetails.bankDetail !== null) {
      return this.fb.group({
        bankName: [this.loggedInUserDetails.bankDetail.docName],
        accountNumber: [this.loggedInUserDetails.bankDetail.accountNumber],
        ifscCode: [this.loggedInUserDetails.bankDetail.ifscCode],
        branchName: [this.loggedInUserDetails.bankDetail.branchName],
      });
    } else {
      return this.fb.group({
        bankName: [''],
        accountNumber: [''],
        ifscCode: [''],
        branchName: [''],
      });
    }
  }
  initKycDetails() {
    if (this.loggedInUserDetails.kycDetail !== null) {
      return this.fb.group({
        docName: [this.loggedInUserDetails.kycDetail.docName],
        docNumber: [this.loggedInUserDetails.kycDetail.docNumber],
      });
    } else {
      return this.fb.group({
        docName: [''],
        docNumber: [''],
      });
    }

  }
  formSubmit() {

    const reqMap = {
      ...this.userForm.value
    };


    this.lotteryService.makeRequestApi('post', 'clientDetails', reqMap).subscribe((res) => {
      if (!res.isError) {
        this.storageService.set('currentUser', Object.assign(this.storageService.get('currentUser'), res.data));
        Object.assign(this.loggedInUserDetails, res.data);

        this.initForm();
        this.isEdit = false;

        this.dataSharingService.updateUserDetails(res.data);
      } else {
        this.alertService.swalSuccess(res.message);
      }
    }, err => {
      // this.alertService.swalSuccess(res.message);
    });

  }
  // addClientDetails() {

  //   let reqMap = {
  //     ...this.personalDetailsForm.value,
  //     userId: this.storageService.get('currentUser'),
  //     status: {
  //       id: 1
  //     }
  //   };
  //   console.log(reqMap);
  //   this.lotteryService.makeRequestApi('post', 'personalDetail', reqMap).subscribe((res) => {
  //     this.alertService.swalSuccess('Client Successfully Added');
  //   }, err => {
  //     console.log(err);
  //   });

  // }


  // addBankDetails() {


  //   let reqMap = {
  //     bankDetails: this.bankDetailsForm.value,
  //     userId: this.storageService.get('currentUser'),
  //     status: {
  //       id: 1
  //     }
  //   };
  //   console.log(reqMap);
  //   this.lotteryService.makeRequestApi('post', 'bankDetails', reqMap).subscribe((res) => {
  //     console.log(res);
  //     this.alertService.swalSuccess('Bank Details Successfully Added');
  //   }, err => {
  //     console.log(err);
  //   });

  // }

  // addKycDetails() {


  //   let reqMap = {
  //     kycDetails: this.kycDetailsForm.value,
  //     userId: this.storageService.get('currentUser'),
  //     status: {
  //       id: 1
  //     }
  //   };
  //   console.log(reqMap);
  //   this.lotteryService.makeRequestApi('post', 'kycDetails', reqMap).subscribe((res) => {
  //     console.log(res);
  //     this.alertService.swalSuccess('KYC Successfully Added');
  //     this.router.navigate(['/client/user-profile']);
  //   }, err => {
  //     console.log(err);
  //   });

  // }
}
