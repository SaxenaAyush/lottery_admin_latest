import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { DatePipe } from '@angular/common';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { Router } from '@angular/router';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import * as $ from '../../../../../node_modules/jquery';
import { ExcelService } from '../../../services/excel.service';

@Component({
  selector: 'app-distributor-daily-report',
  templateUrl: './distributor-daily-report.component.html',
  styleUrls: ['./distributor-daily-report.component.scss']
})
export class DistributorDailyReportComponent implements OnInit {
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  id= this.userD.user.roleMaster.id;
  navModel: any = {
    "playerPlayed": 1,
    "dailyReport": 2,
    "selfReport": 1,
    "walletReport": 1,
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.playerPlayed = 2;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 1;

    }
    else if (input == 2) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 2;
      this.navModel.walletReport = 1;

    }
    else if (input == 3) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 2;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 1;

    
    }
    else if (input == 4) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 2;

    
    }
  }
  dailyReportFiltered:any[]=[];
  dailyReport:any[]=[];
  allRetailers:any[]=[];
  TodayDate:any;
  PlayerTodayDate:any;
  today = Date();
  page = 1;
  pageSize = 7;
  constructor(private alertService: SweetAlertService, private datePipe: DatePipe, 
    private lotteryHttpService: LotteryHttpService, private route: Router,
     private storageService: LotteryStorageService,private excelService:ExcelService) { }

  ngOnInit() {
    this.Dailyreport();
  }
  viewPlayerOnline(){
    this.route.navigate(['/distributor/distributor-report'])
  }
  report(){
    this.route.navigate(['/distributor/distributor-self-report'])

  }
  Walletreport(){
    this.route.navigate(['/distributor/wallet-History'])

  }

Dailyreport(){


  let reqMap = {
    bookingDate: this.datePipe.transform(this.today, "dd-MM-yyyy"),
  }
  this.lotteryHttpService.makeRequestApi('post', 'bookingvsClaim', reqMap).subscribe((res) => {
    this.allRetailers = res.content;
    this.dailyReportFiltered = this.allRetailers

  
    console.log(this.dailyReport);

  });

}
dailyReportByDate(){
  let reqMap = {
    bookingDate: this.datePipe.transform(this.TodayDate, "dd-MM-yyyy") ,
  
  }
  this.lotteryHttpService.makeRequestApi('post', 'bookingvsClaim', reqMap).subscribe((res) => {
    this.allRetailers = res.content;
    this.dailyReportFiltered = this.allRetailers
  
    console.log(this.dailyReport);

  }); 
}
exportAsXLSX():void {
  this.excelService.exportAsExcelFile(this.dailyReportFiltered, 'sample');
}
Distributor(){
  $('.distributor-Form').show();
  $('.dailyReport-Form').hide();

}
Subdistributor(){
  $('.distributor-Form').show();
  $('.dailyReport-Form').hide();

}
}
