import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import * as $ from '../../../../../node_modules/jquery';

@Component({
  selector: 'app-distributor-report',
  templateUrl: './distributor-report.component.html',
  styleUrls: ['./distributor-report.component.scss']
})
export class DistributorReportComponent implements OnInit {
  viewReportDetails:any[]=[];
  FromDate: any;
  dailyReportFiltered:any[]=[];
  filteredRetailerList:any[]=[];
  allRetailers:any[]=[];
  playerOnlineList:any[]=[];
  filteredplayerOnlineList:any[]=[];
  ToDate: any;
  page = 1;
  pageSize = 7;
  page2 = 1;
pageSize2 = 8;
  TodayDate:any;
  PlayerTodayDate:any;
  today = Date();
  navModel: any = {
    "playerPlayed": 2,
    "dailyReport": 1,
    "selfReport": 1,
    "walletReport": 1,
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.playerPlayed = 2;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 1;

    }
    else if (input == 2) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 2;
      this.navModel.walletReport = 1;

    }
    else if (input == 3) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 2;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 1;

    
    }
    else if (input == 4) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 2;

    
    }
  }
 
  List :any = [];
  constructor(private alertService: SweetAlertService, private datePipe: DatePipe, 
    private lotteryHttpService: LotteryHttpService, private route: Router,
     private storageService: LotteryStorageService) { }

  ngOnInit() {
    $('.dailySubDistributor-Form').hide();

    this.viewPlayerOnline();
  }
 
report(){
  // $('.playerPlayedForm').hide();
  // $('.selfReportForm').show();
  // $('.dailyReportForm').hide();
  this.route.navigate(['/distributor/distributor-self-report'])
}
// player(){
//   $('.dailySubDistributorForm').show();
//   $('.selfReportForm').hide();
//   $('.dailyReportForm').hide();

// }
dailyReport:any[]=[];
dailyreport(){
  this.route.navigate(['/distributor/distributor-daily-report'])


}

playerDate = this.today;
viewPlayerOnline() {
  $('.playerPlayedForm').show();
  $('.selfReportForm').hide();
  $('.dailyReportForm').hide();

  let reqMap = {
    reportDate: this.datePipe.transform(this.playerDate, "dd-MM-yyyy") ,
    userId: this.storageService.get('currentUser').user.id,

  }
  this.lotteryHttpService.makeRequestApi('post', 'allUserCreatedBy', reqMap).subscribe((res) => {
    this.playerOnlineList = res.content;
    this.filteredplayerOnlineList = this.playerOnlineList
  
    console.log('filteredplayerOnlineList',this.filteredplayerOnlineList);

  }); 
 
  }
  viewPlayerOnlineByDate() {
    this.playerDate = this.PlayerTodayDate
    let reqMap = {
      reportDate: this.datePipe.transform(this.playerDate, "dd-MM-yyyy") ,
      userId: this.storageService.get('currentUser').user.id,

    }
    
    this.lotteryHttpService.makeRequestApi('post', 'allUserCreatedBy', reqMap).subscribe((res) => {
      this.playerOnlineList = res.content;
      this.filteredplayerOnlineList = this.playerOnlineList
    
      console.log('filteredplayerOnlineList',this.filteredplayerOnlineList);
  
    }); 
    }
    viewPlayerDetails:any[]=[];
    deepDetailsClient:any[]=[];
    firstName:any;
    lastName:any;
    userCode:any;

    viewClient(emp: any,first:any,last:any,code:any) {
      this.firstName = first,
      this.lastName= last
      this.userCode = code
      this.lotteryHttpService.makeRequestApi('post', 'allUserCreatedById', {
        retailerId: emp,
        bookingDate: this.datePipe.transform(this.playerDate, "dd-MM-yyyy") ,
      }).subscribe(res => {
      console.log('hello user data', res);
      this.deepDetailsClient = res.content
      this.storageService.set('deepDetails', this.deepDetailsClient);
      this.storageService.set('first', this.firstName);
      this.storageService.set('last', this.lastName);
      this.storageService.set('date', this.datePipe.transform(this.playerDate, "dd-MM-yyyy"));
      this.storageService.set('code', this.userCode);

      this.route.navigate(['/distributor/view-Transaction-report']);
      });
      }
  searchDataByPrice: any;
  onSearchFilterActivate(searchData) {
      if (searchData) {
        this.dailyReportFiltered = this.dailyReport.filter(item => {
          const filter = Object.keys(item);
          return filter.some(
            key => {
              if ((key === 'drawTimeAm' || key === 'totalPrice' || key === 'bookingDate') && item[key] != null) {
                return item[key].toLowerCase().indexOf(searchData.toLowerCase()) !== -1;
              }
              else if (( key === 'winningPrice' ) && item[key] != null) {
                return item[key].toString().indexOf(searchData.toString()) !== -1;
              }
            }
          );
        });
      } else {
        this.dailyReportFiltered = this.dailyReport;
      }
    }
    Walletreport(){
      this.route.navigate(['distributor/wallet-History'])
    }
  
}
