import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewDeactivatedclientComponent } from './view-Deactivatedclient.component';


describe('ViewDeactivatedclientComponent', () => {
  let component: ViewDeactivatedclientComponent;
  let fixture: ComponentFixture<ViewDeactivatedclientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDeactivatedclientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDeactivatedclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
