import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { LotteryStorageService } from '../../../services/lottery-storage.service';

@Component({
  selector: 'app-wallet-history',
  templateUrl: './wallet-history.component.html',
  styleUrls: ['./wallet-history.component.scss']
})
export class WalletHistoryComponent implements OnInit {
  navModel: any = {
    "playerPlayed": 1,
    "dailyReport": 1,
    "selfReport": 1,
    "walletReport": 2,
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.playerPlayed = 2;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 1;

    }
    else if (input == 2) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 2;
      this.navModel.walletReport = 1;

    }
    else if (input == 3) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 2;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 1;

    
    }
    else if (input == 4) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 2;

    
    }
  }
  walletlist :any[]=[];
  walletlistFiltered:any[]=[];
  TodayDate:any;
  PlayerTodayDate:any;
  today = Date();
  page = 1;
  pageSize = 8;
  constructor(private alertService: SweetAlertService, private datePipe: DatePipe, 
    private lotteryHttpService: LotteryHttpService, private route: Router,
     private storageService: LotteryStorageService) { }

  ngOnInit() {
    this.Walletreport();
  }
  viewPlayerOnline(){
    this.route.navigate(['/distributor/distributor-report'])

  }
  report(){
    this.route.navigate(['/distributor/distributor-self-report'])

  }
  Dailyreport(){
    this.route.navigate(['/distributor/distributor-daily-report'])

  }
  Walletreport(){


    let reqMap = {
      user : {
        id: this.storageService.get('currentUser').user.id,

        },
        transactionDate  : this.datePipe.transform(this.playerDate, "dd-MM-yyyy"),
     

    }
    console.log('Walletreport',reqMap)

    this.lotteryHttpService.makeRequestApi('post', 'walletHistory', reqMap).subscribe((res) => {
      this.walletlist = res.content;
      this.walletlistFiltered = this.walletlist
  
    
  
    });
  
  }
  walletReportByDate(){
    this.playerDate = this.PlayerTodayDate

    let reqMap = {
      user : {
        id: this.storageService.get('currentUser').user.id,

        },
        transactionDate  : this.datePipe.transform(this.playerDate, "dd-MM-yyyy"),    

    }
    console.log('walletReportByDate',reqMap)
    this.lotteryHttpService.makeRequestApi('post', 'walletHistory', reqMap).subscribe((res) => {
      this.walletlist = res.content;
      this.walletlistFiltered = this.walletlist
    
  
    }); 
  }
  firstName:any;
  lastName:any;
  userCode:any;
  id:any;
  deepDetails:any[]=[];
  playerDate = this.today;

  viewRetailerHistory(emp:any,first:any,last:any,code:any){
    this.firstName = first,
    this.lastName= last
    this.userCode = code
    this.id = emp
    console.log('Ea Id hai ',this.id);
    this.lotteryHttpService.makeRequestApi('post', 'retailerWalletHistory', {
      transactionDate :this.datePipe.transform(this.playerDate, "dd-MM-yyyy"),
      user: {
        id:emp //retailer id
      }
      }).subscribe(res => {
      console.log('hello user data', res);
      this.deepDetails = res.content

      this.storageService.set('retailerTransactionHistory', res);
      this.storageService.set('deepDetails', this.deepDetails);
      this.storageService.set('first', this.firstName);
      this.storageService.set('last', this.lastName);
      this.storageService.set('date', this.datePipe.transform(this.playerDate, "dd-MM-yyyy"));
      this.storageService.set('code', this.userCode);
      this.storageService.set('empid', this.id);

      this.route.navigate(['/distributor/retailer-wallet-report']);
      });
  }
}
