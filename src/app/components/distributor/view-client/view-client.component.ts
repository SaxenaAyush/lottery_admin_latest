import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from '../../../../../node_modules/jquery';
import Swal from 'sweetalert2';
import { SweetAlertService } from '../../../../../src/app/common/sharaed/sweetalert2.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import getMAC, { isMAC } from 'getmac';
import moment from 'moment';
import { DatePipe } from '@angular/common';
import * as jsPDF from 'jspdf';
import { ExcelService } from '../../../services/excel.service';
@Component({
selector: 'app-view-client',
templateUrl: './view-client.component.html',
styleUrls: ['./view-client.component.scss']
})
export class ViewClientComponent implements OnInit {
@ViewChild('walletModal') walletModal: any;
@ViewChild('licenseModal') licenseModal: any;
@ViewChild('deductWalletModal') deductWalletModal: any;

userD = this.storageService.get('currentUser');
roleId= this.userD.user.roleMaster.id;
clientId: any;
amount1: any;
deductAmount:any;
page = 1;
pageSize = 8;
loggedInUserDetails: any;
allClients: any = [];
viewClientDetails: any = [];
Client: any = [];
allInActiveClients: any = [];
userName = this.userD.user.userName;
phoneNumber = this.userD.user.phoneNumber;
phoneDisp = "(" + this.phoneNumber + ")";
role = this.userD.user.roleMaster.name;
public walletForm: FormGroup;
public licenseForm: FormGroup;
today: number = Date.now();
filteredEmployeeList: any[] = [];
data: any = [{
  eid: 'e101',
  ename: 'ravi',
  esal: 1000
},
{
  eid: 'e102',
  ename: 'ram',
  esal: 2000
},
{
  eid: 'e103',
  ename: 'rajesh',
  esal: 3000
}];
id: any;
constructor(private excelService:ExcelService ,private datePipe: DatePipe, private sweetAlert: SweetAlertService, private http: HttpClient, private fb: FormBuilder, private storageService: LotteryStorageService, private router: Router, private alertService: SweetAlertService, private lotteryService: LotteryHttpService) {
}

allClientsDetailsList :any[]=[];
ngOnInit() {
console.log(getMAC())
$('.activated-Form').show();
$('.deactivated-Form').hide();
// console.log("ip");
// this.userService.getIpAddress().subscribe(data => {
// console.log(data);
// });
this.getEmployee();
this.initLicenseForm();
// this.allClientsDetailsList = this.allClientsDetails;
// console.log('netaji',this.allClientsDetails[0].roleId)
// roleId = this.allInActiveClients
}
exportAsXLSX():void {
  this.excelService.exportAsExcelFile(this.data, 'sample');
}
initWalletForm() {
this.walletForm = this.fb.group({
phoneNumber: [''],
comment: [''],
name: [''],
transactionNo: [''],
chequeNo: ['']
});
}

initLicenseForm() {
this.licenseForm = this.fb.group({
fromDate: [''],
toDate: [''],
})
}
openAddClient() {
this.router.navigate(['/distributor/add-new-client']);
}

openActivatedClient() {
$('.activated-Form').show();
$('.deactivated-Form').hide();
$('.activated-Form').show();
$('.distributor-Form').hide();
$('.subDistributor-Form').hide();
$('.admin-Form').hide();
this.router.navigate(['/distributor/view-client']);
}

openDeactivatedClient() {
$('.activated-Form').hide();
$('.deactivated-Form').show();
console.log('Deactivated');
this.router.navigate(['/distributor/view-Deactivatedclient-details']);
}

deleteClient(emp: any) {
console.log('emp', emp)
Swal.fire({
html: '<h3 class="mt-5 mb-4 mx-2">Are you sure you want to deactivate this client?</h3>',
animation: true,
width: 548,
padding: '-1.9em',
showCancelButton: true,
focusConfirm: false,
cancelButtonText: 'No',
cancelButtonColor: '#17A2B8',
cancelButtonClass: 'btn btn-outline-info btn-pill px-4 mr-2',
confirmButtonColor: '#DD6B55',
confirmButtonClass: 'btn btn-danger btn-pill px-4',
confirmButtonText: 'Yes'
}).then((result) => {
if (result.value) {
this.lotteryService.makeRequestApi('post', 'deleteClient', {

id: emp,
status: 2
}).subscribe(res => {
if (!res.isError) {
this.getEmployee();
}
});
}
});

}

activeClient(emp: any) {
console.log('emp', emp)
Swal.fire({
html: '<h3 class="mt-5 mb-4 mx-2">Are you sure you want to Activate this client?</h3>',
animation: true,
width: 548,
padding: '-1.9em',
showCancelButton: true,
focusConfirm: false,
cancelButtonText: 'No',
cancelButtonColor: '#17A2B8',
cancelButtonClass: 'btn btn-outline-info btn-pill px-4 mr-2',
confirmButtonColor: '#DD6B55',
confirmButtonClass: 'btn btn-danger btn-pill px-4',
confirmButtonText: 'Yes'
}).then((result) => {
if (result.value) {
this.lotteryService.makeRequestApi('post', 'deleteClient', {

id: emp,
status: 1
}).subscribe(res => {
if (!res.isError) {
this.getEmployee();
}
});
}
});

}
getEmployee() {

const reqMap = {
id: this.storageService.get('currentUser').user.id,

}
this.lotteryService.makeRequestApi('post', 'getAllUserListByCreator', {
id: this.storageService.get('currentUser').user.id,

}).subscribe(res => {

this.Client = res;
console.log('client check', this.Client);
this.allClients = this.Client.content.Active;
this.filteredEmployeeList = this.allClients;
this.allInActiveClients = this.Client.content.InActive;
console.log('all client check ', this.allClients);
});

}
viewClient(emp: any) {
this.lotteryService.makeRequestApi('post', 'viewClient', {
id: emp,
status: 1
}).subscribe(res => {
console.log('hello user data', res);
this.storageService.set('viewUser', res);
this.viewClientDetails = res;
this.router.navigate(['/distributor/view-client-details']);
});
}
openCash() {
$('.cash').show();
$('.neft').hide();
$('.cheque').hide();

}
opencheque() {
$('.cash').hide();
$('.neft').hide();
$('.cheque').show();

}
openNeft() {
$('.cash').hide();
$('.neft').show();
$('.cheque').hide();

}
// userId(emp) {
// this.id = emp
// console.log(this.id);

// }
Update() {

console.log('from date', this.userModel.fromDate);
console.log('to date', this.userModel.toDate);
let reqMap = {
fromDate: this.userModel.fromDate,
toDate: this.userModel.toDate,
loginId: this.storageService.get('currentUser').user.id,
userId: this.id
};
reqMap['fromDate'] = moment(reqMap['fromDate']).format('DD-MM-YYYY');
reqMap['toDate'] = moment(reqMap['toDate']).format('DD-MM-YYYY');
console.log(reqMap);
this.lotteryService.makeRequestApi('post', 'licenceRenewal', reqMap).subscribe(res => {
console.log('hello user data', res);


});
this.licenseModal.hide();
this.router.navigate(['/distributor/view-client']);
this.alertService.swalSuccess('updated successfully');
}

machineReset() {
let reqMap = {
userId: this.id,
};

console.log(reqMap);
this.lotteryService.makeRequestApi('post', 'resetMachine', reqMap).subscribe(res => {
console.log('hello user data', res);

this.router.navigate(['/distributor/view-client']);
});
this.router.navigate(['/distributor/view-client']);
this.licenseModal.hide();
}

addMoney() {
let reqMap = {


// "fromId": this.storageService.get('currentUser').user.id,
// "toId": this.clientId,
amount: this.amount1,
// transactionType: "cash",
// comment: "wallet transfer",

requestBy: {
id: this.storageService.get('currentUser').user.id
},
requestTo: {
id: this.clientId
},
// paymentMode: "cash",
// bankName: "united",
// remarks: "remarkkk",
// chequeNo: "2383388383",
// bankTransactionId: "83kdi3ikdkd",
// name: "ranjeet",
// amount: this.amount,
// phoneNo: 438383838
}
console.log(reqMap);
this.lotteryService.makeRequestApi('post', 'Transfer', reqMap).subscribe((res) => {
if (res.code == 206) {
this.sweetAlert.swalSuccess('Amount Transfered');
this.walletModal.hide();
} else if (res.code == 208) {
this.sweetAlert.swalError(res.message);
}
else if (res.code == 201) {
this.sweetAlert.swalError(res.message);
}

});
this.amount1 = "";
}
walletOpenModal(id: any) {
this.walletModal.show();
this.clientId = id;
}
licenseDetailsList: any;

userModel: any = {
"fromDate": "",
"toDate": "",
"MachineKey": "",
"licenseKey": "",

}

type= "text";
change() {
if(this.type=="date"){
this.type = "text";
this.userModel.fromDate = this.licenseDetailsList.fromDate;
this.userModel.toDate = this.licenseDetailsList.toDate;
}else if(this.type=="text"){
this.type = "date";
this.userModel.fromDate = "";
this.userModel.toDate = "";
}

}
datestatus =1;
checkdate(){
let a: any;
let b: any;
let cur: number | Date;
// reqMap['fromDate'] = moment(reqMap['fromDate']).format('DD-MM-YYYY');
let current = moment(new Date(this.userModel.fromDate)).format('DD-MM-YYYY');
// var ddMMyyyy = this.datePipe.transform(new Date(this.userModel.fromDate),"dd-MM-yyyy");
cur = new Date(current);
// console.log('current ddMMyyyy',ddMMyyyy);
console.log('this.userModel.toDate',this.userModel.fromDate);


// let myDate:Date = moment(dateString).format("DD/MM/YYYY");
let dateObject = moment(this.userModel.fromDate, "DD-MM-YYYY").toDate();
console.log('current dateObject',dateObject);
//last date
let lst: Date;
// let last = moment(new Date(this.userModel.toDate)).format('DD-MM-YYYY');
let last = moment(new Date(this.userModel.toDate)).format('DD-MM-YYYY');
console.log('last', last) 
lst = new Date(last);
console.log('last date', lst)



a= lst.setDate(lst.getDate() - 7);
let before = new Date(a);
console.log('current a',a);
//console.log('this.userModel.fromDate',this.userModel.fromDate);
//console.log('current',current);
console.log(' before',before);
if(cur>=before){
this.datestatus =2;
}else{
this.datestatus =1;
}

}

licenseDetails(emp: any) {
this.id = emp
let reqMap = {

userId: this.id

}
if (this.type == "date") {
this.type = "text";

}
this.lotteryService.makeRequestApi('post', 'getLicense', reqMap).subscribe((res) => {
this.licenseDetailsList = res.content;
console.log(this.licenseDetailsList);
this.userModel.fromDate = this.licenseDetailsList.fromDate,
console.log('from date', this.userModel.fromDate);
this.userModel.toDate = this.licenseDetailsList.toDate,
console.log('todate', this.userModel.toDate);
this.userModel.MachineKey = this.licenseDetailsList.machinekey,
console.log(this.userModel.MachineKey);
this.userModel.licenseKey = this.licenseDetailsList.licenceKey,
console.log(this.userModel.licenseKey);
this.checkdate();
});
this.licenseModal.show();
}

sixMonths() {

var d = new Date(this.userModel.fromDate);
console.log('before',d);
d.setMonth(d.getMonth() + 6);
console.log('after',d);
this.userModel.todate = d;

}
eightMonths() {
var d = new Date();
d.setMonth(d.getMonth() + 8);
console.log(d);
}
twelveMonths() {
var d = new Date();
d.setMonth(d.getMonth() + 12);
console.log(d);
}


searchDataActivate: any;
onSearchFilterActivate(searchData) {
    if (searchData) {
      this.filteredEmployeeList = this.allClients.filter(item => {
        const filter = Object.keys(item);
        return filter.some(
          key => {
            if ((key === 'firstName' || key === 'email' || key === 'lastName' || key === 'phoneNumber') && item[key] != null) {
              return item[key].toLowerCase().indexOf(searchData.toLowerCase()) !== -1;
            }
          }
        );
      });
    } else {
      this.filteredEmployeeList = this.allClients;
    }
  }
  firstName:any;
  lastName:any;
  userCode:any;
  Id:any;
  deepDetails:any[]=[];
  playerDate = this.today;


  viewRetailerTransactionHistory(emp:any,first:any,last:any,code:any){
    this.firstName = first,
    this.lastName= last
    this.userCode = code
    this.Id = emp
    console.log('Ea Id hai ',this.id);
    this.lotteryService.makeRequestApi('post', 'retailerWalletHistory', {
      transactionDate :this.datePipe.transform(this.today, "dd-MM-yyyy"),
      user: {
        id:emp //retailer id
      }
      }).subscribe(res => {
      console.log('hello user data', res);
      this.deepDetails = res.content

      this.storageService.set('retailerTransactionHistory', res);
      this.storageService.set('deepDetails', this.deepDetails);
      this.storageService.set('first', this.firstName);
      this.storageService.set('last', this.lastName);
      this.storageService.set('date', this.datePipe.transform(this.playerDate, "dd-MM-yyyy"));
      this.storageService.set('code', this.userCode);
      this.storageService.set('empid', this.id);

      this.router.navigate(['/distributor/retailer-wallet-report']);
      });
  }
  viewDetails(emp: any) {
    // this.lotteryHttpService.makeRequestApi('post', 'viewClient', {
    // id: emp,
    // status: 1
    // }).subscribe(res => {
    // console.log('hello user data', res);
    // this.storageService.set('viewUser', res);
    // this.viewReportDetails = res;
    // this.route.navigate(['/distributor/view-client-details']);
    // });
    this.router.navigate(['/distributor/wallet-History']);
    }
    empId:any;
    viewDeductDetails:any;
    openDeductModal(emp: any){
      this.deductWalletModal.show();
     this.empId = emp,
     console.log('Letata',this.empId,this.deductAmount);
    }
    deductBalance() {

      this.lotteryService.makeRequestApi('post', 'deductBalance', {
        user:{
          id: this.empId
        },
        toId: this.storageService.get('currentUser').user.id,
        transactionAmount: this.deductAmount,
        comment:"deduct by admin",
      
      }).subscribe(res => {
        if(res.code == 206){
          console.log('hello user data', res);
          // this.storageService.set('viewUser', res);
          this.viewDeductDetails = res;
          this.router.navigate(['/distributor/view-client']);
          this.sweetAlert.swalSuccess('Balance Deducted Successfully');
          this.deductWalletModal.hide();
          this.deductAmount="";
        } else{
          this.sweetAlert.swalError('Error Occurred');
          this.deductAmount="";

        }
  
      });
  
      }
      viewAdminsDetails:any[]=[];
      viewDistributorDetails:any[]=[];
      viewSubDistributorDetails:any[]=[];
      firstNameA:any;
      lastNameA:any;
      firstNameAd:any;
      lastNameAd:any;
      firstNameB:any;
      lastNameB:any;
      SubemployeesID:any
      DisemployeesID:any;
      adminemployeesID:any;
      viewAdmins(emp3: any,first:any,last:any) {
        this.firstNameAd = first,
        this.lastNameAd = last,
        this.adminemployeesID = emp3
        console.log("viewSubDistributorDetails");
        $('.activated-Form').hide();
        $('.admin-Form').show();
        $('.distributor-Form').hide();
        $('.subDistributor-Form').hide();
        this.lotteryService.makeRequestApi('post', 'getAllUserListByCreator', {
          id: emp3,
          
          }).subscribe(res => {
          
        console.log('hello user data', res);
        this.viewAdminsDetails = res.content.Active;
        this.storageService.set('viewAdminsDetails', this.viewAdminsDetails);
        this.storageService.set('admin', this.adminemployeesID);

        });
        }
      viewDistributors(emp: any,first:any,last:any) {
      this.firstNameA = first,
      this.lastNameA = last,
      this.DisemployeesID = emp

      console.log('name',this.firstNameA,this.lastNameA);
        $('.activated-Form').hide();
        $('.distributor-Form').show();
        $('.subDistributor-Form').hide();
        $('.admin-Form').hide();
          this.lotteryService.makeRequestApi('post', 'getAllUserListByCreator', {
          id: emp,
          
          }).subscribe(res => {
          
        console.log('hello user data', res);
        this.viewDistributorDetails = res.content.Active;
        this.storageService.set('viewDistributorDetails', this.viewDistributorDetails);
        this.storageService.set('employeesID', this.DisemployeesID);

        });
        }
        viewSubDistributors(emp2: any,first:any,last:any) {
          this.firstNameB = first,
          this.lastNameB = last,
          this.SubemployeesID = emp2
          this.storageService.set('SubemployeesID', this.SubemployeesID);

          console.log("viewSubDistributorDetails");
          $('.activated-Form').hide();
          $('.distributor-Form').hide();
          $('.subDistributor-Form').show();
          $('.admin-Form').hide();
          this.lotteryService.makeRequestApi('post', 'getAllUserListByCreator', {
            id: emp2,
            
            }).subscribe(res => {
            
          console.log('hello user data', res);
          this.viewSubDistributorDetails = res.content.Active;
          this.storageService.set('viewSubDistributorDetails', this.viewSubDistributorDetails);
        });
          }
          DistributorPage(){
    
      console.log('name',this.firstNameA,this.lastNameA);
        $('.activated-Form').hide();
        $('.distributor-Form').show();
        $('.subDistributor-Form').hide();
        $('.admin-Form').hide();
          this.lotteryService.makeRequestApi('post', 'getAllUserListByCreator', {
          id: this.DisemployeesID,
          
          }).subscribe(res => {
          
        console.log('hello user data', res);
        this.viewDistributorDetails = res.content.Active;
        this.storageService.set('viewDistributorDetails', this.viewDistributorDetails);
        this.storageService.set('employeesID', this.DisemployeesID);

        });
          }
          AdminPage(){
         
            console.log("viewSubDistributorDetails");
            $('.activated-Form').hide();
            $('.admin-Form').show();
            $('.distributor-Form').hide();
            $('.subDistributor-Form').hide();
            this.lotteryService.makeRequestApi('post', 'getAllUserListByCreator', {
              id: this.adminemployeesID,
              
              }).subscribe(res => {
              
            console.log('hello user data', res);
            this.viewAdminsDetails = res.content.Active;
            this.storageService.set('viewAdminsDetails', this.viewAdminsDetails);
            this.storageService.set('admin', this.adminemployeesID);
    
            });
          }

          openChangeParent(){
            this.router.navigate(['/distributor/change-parent']);
          }

        }