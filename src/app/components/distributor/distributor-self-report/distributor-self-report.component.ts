import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { LotteryStorageService } from '../../../services/lottery-storage.service';

@Component({
  selector: 'app-distributor-self-report',
  templateUrl: './distributor-self-report.component.html',
  styleUrls: ['./distributor-self-report.component.scss']
})
export class DistributorSelfReportComponent implements OnInit {
  claimAmount:any;
  currentBalance:any;
  name:any;
  today = Date();
  payToCompany:any;
  profit:any;
  ticketBooked:any;
  totalWalletRecharge:any;
  cashInDrawer:any;
  reportDate:any;
  closingBalance:any;
  openingBalance:any;
  durationclaimAmount:any;
  durationcurrentBalance:any;
  durationname:any;
  durationpayToCompany:any;
  durationprofit:any;
  durationticketBooked:any;
  durationtotalWalletRecharge:any;
  durationcashInDrawer:any;
  durationreportDate:any;
  durationclosingBalance:any;
  durationopeningBalance:any;
  durationdailyRecharge:any[]=[];
  dailyRecharge:any = [];
  bookingslot: any;
  SlotstartTime: any;
  SlotEndTime: any;
  slots: any;
  status = 1;
  FromDate: any;
  ToDate: any;
  FromSlot: any;
  ToSlot: any;
  detailsList: any;
  Claim: any;
  ClaimBonus: any;
  Commission: any;
  TicketBooked: any;
  WalletBalance:any;
  Profit:any;
  navModel: any = {
    "playerPlayed": 1,
    "dailyReport": 1,
    "selfReport": 2,
    "walletReport": 1,
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.playerPlayed = 2;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 1;

    }
    else if (input == 2) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 2;
      this.navModel.walletReport = 1;

    }
    else if (input == 3) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 2;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 1;

    
    }
    else if (input == 4) {
      this.navModel.playerPlayed = 1;
      this.navModel.dailyReport = 1;
      this.navModel.selfReport = 1;
      this.navModel.walletReport = 2;

    
    }
  }
  constructor(private alertService: SweetAlertService, private datePipe: DatePipe, 
    private lotteryHttpService: LotteryHttpService, private route: Router,
     private storageService: LotteryStorageService) { }

  ngOnInit() {
    this.getDetailsToday();
  }
  Walletreport(){
    this.route.navigate(['/distributor/wallet-History'])

  }
  dailyreport(){
    this.route.navigate(['/distributor/distributor-daily-report'])

  } 
  viewPlayerOnline(){
    this.route.navigate(['/distributor/distributor-report'])

  }
  getDetails() {
    console.log('Netaji');

    console.log('Netaji',this.ToDate);
    let reqMap = {
      userId: this.storageService.get('currentUser').user.id,
      reportDate: this.datePipe.transform(this.ToDate, "dd-MM-yyyy"),
    
    }
 
    this.lotteryHttpService.makeRequestApi('post', 'dailyReport', reqMap).subscribe(res => {
      this.storageService.set('reportDetails', res);
      this.detailsList = res.content;
      this.storageService.set('reportDetails', this.detailsList);
      this.claimAmount = this.detailsList.claimAmount,
      this.currentBalance = this.detailsList.currentBalance,
      this.payToCompany = this.detailsList.payToCompany,
      this.ticketBooked = this.detailsList.ticketBooked 
      this.cashInDrawer = this.detailsList.cashAmount
      this.profit = this.detailsList.profit,
      this.totalWalletRecharge = this.detailsList.totalWalletRecharge,
      this.reportDate = this.detailsList.reportDate,
      this.name = this.detailsList.name,
      this.dailyRecharge = this.detailsList.dailyRecharge,
      this.closingBalance = this.detailsList.closingBalance,
      this.openingBalance = this.detailsList.openingBalance
      console.log('Paisaaaa',this.dailyRecharge);
      // this.List.push({ Claim : this.detailsList.Claim,
      //   ClaimBonus : this.detailsList.ClaimBonus,
      //   Commission : this.detailsList.Commission,
      //   TicketBooked : this.detailsList.TicketBooked });

    });
    }
    getDetailsToday() {
      console.log('Netaji');
  
      console.log('Netaji',this.ToDate);
      let reqMap = {
        userId: this.storageService.get('currentUser').user.id,
        reportDate: this.datePipe.transform(this.today, "dd-MM-yyyy"),
      
      }
   
      this.lotteryHttpService.makeRequestApi('post', 'dailyReport', reqMap).subscribe(res => {
        this.storageService.set('reportDetails', res);
        this.detailsList = res.content;
        this.storageService.set('reportDetails', this.detailsList);
        this.claimAmount = this.detailsList.claimAmount,
        this.currentBalance = this.detailsList.currentBalance,
        this.payToCompany = this.detailsList.payToCompany,
        this.ticketBooked = this.detailsList.ticketBooked 
        this.cashInDrawer = this.detailsList.cashAmount
        this.profit = this.detailsList.profit,
        this.totalWalletRecharge = this.detailsList.totalWalletRecharge,
        this.reportDate = this.detailsList.reportDate,
        this.name = this.detailsList.name,
        this.dailyRecharge = this.detailsList.dailyRecharge,
        this.closingBalance = this.detailsList.closingBalance,
        this.openingBalance = this.detailsList.openingBalance
        console.log('Paisaaaa',this.dailyRecharge);
        // this.List.push({ Claim : this.detailsList.Claim,
        //   ClaimBonus : this.detailsList.ClaimBonus,
        //   Commission : this.detailsList.Commission,
        //   TicketBooked : this.detailsList.TicketBooked });
  
      });
      }
   
}
