import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributorSelfReportComponent } from './distributor-self-report.component';

describe('DistributorSelfReportComponent', () => {
  let component: DistributorSelfReportComponent;
  let fixture: ComponentFixture<DistributorSelfReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributorSelfReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributorSelfReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
