import { Component, OnInit } from '@angular/core';
import * as $ from '../../../../../node_modules/jquery';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';

@Component({
  selector: 'app-change-parent',
  templateUrl: './change-parent.component.html',
  styleUrls: ['./change-parent.component.scss']
})
export class ChangeParentComponent implements OnInit {
  page = 1;
pageSize = 5;
  loggedInUserDetails: any;
  userD = this.storageService.get('currentUser');
 id= this.userD.user.roleMaster.id;
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber
  firstName = this.userD.user.firstName;
  lastName = this.userD.user.lastName;  
status = 1;
allSubDistributor:any[]=[];
allDistributor:any[]=[];
allRetailer:any[]=[];
clientsList:any;
parentChangeList:any;
UserfirstName:any;
UserlastName:any;
AdminfirstName:any;
AdminlastName:any;
roleMasterId:any;
  roleModelId:any;
navModel: any = {
  "reatiler": 2,
  "subDistributors": 1,
  "Distributors": 1,

}
navbar(input: any) {
  console.log(input);
  if (input == 1) {
    this.navModel.reatiler = 2;
    this.navModel.subDistributors = 1;
    this.navModel.Distributors = 1;

  }
  else if (input == 2) {
    this.navModel.reatiler = 1;
    this.navModel.subDistributors = 2;
    this.navModel.Distributors = 1;
  }
  else if (input == 3) {
    this.navModel.reatiler = 1;
    this.navModel.subDistributors = 1;
    this.navModel.Distributors = 2;
  }

  
} 
  constructor(private storageService: LotteryStorageService, private HttpService: LotteryHttpService,private sweetalert: SweetAlertService) { }

  ngOnInit() {
    this.getEmployee();
  }
  single() {
    this.status = 1;
   
  

  }
  duration() {
    this.status = 2;


  }
  changeRetailer(){
    $('.retailer-Form').show();
    $('.subDistributor-Form').hide();
    $('.Distributor-Form').hide();
    $('.subDistributorRetailers-Form').hide();
    $('.DistributorRetailers-Form').hide();

    console.log('Retailer Form');
  }
  changeSubDistributor(){
    $('.retailer-Form').hide();
    $('.subDistributor-Form').show();
    $('.Distributor-Form').hide();
    $('.subDistributorRetailers-Form').hide();
    $('.DistributorRetailers-Form').hide();

    console.log('SubDistributor Form');

  }
  changeDistributor(){
    $('.retailer-Form').hide();
    $('.subDistributor-Form').hide();
    $('.Distributor-Form').show();
    $('.subDistributorRetailers-Form').hide();
    $('.DistributorRetailers-Form').hide();

    console.log('Distributor Form');

  }
  roleModel: any = {
    "id": 0,
    "ID":0
  }
  
  onChange3(event){
    for(let get of this.allSubDistributor){
      if(get.id==this.roleModel.id){
        console.log('role id',get.roleMaster.id);
        console.log('roleModal id',get.id);
        this.roleMasterId = get.roleMaster.id,
        this.roleModelId = get.id
      }
    }
    console.log('event', event);

    // this.roleModel.id=0;
    // this.roleMasterId=0;
    // this.roleModel.id=event.target.value;
    // this.roleMasterId=event.target.value;
    // console.log('roleModel',this.roleModel.id);
  
   

  }
  onChange1(event1){
    console.log('event', event1);

    console.log('roleModel',event1);
   

  }
  roleid(id: any,ID:any) {
   
  
  }
  selectedDevice : any;
  onChange(newValue) {
  
    console.log(newValue);
    this.selectedDevice = newValue;
    
}
  getEmployee() {

    const reqMap = {
    roleMaster: {
        id: this.id
    },
    id: this.storageService.get('currentUser').user.id,

    }
    this.HttpService.makeRequestApi('post', 'myAllEmployee', reqMap).subscribe(res => {
    
    this.clientsList = res;
    console.log('client check', this.clientsList);
    this.allRetailer= this.clientsList.content.RetailerList;
    this.allSubDistributor = this.clientsList.content.SubdistributorList;
    this.allDistributor = this.clientsList.content.DistributorList;
    console.log('all allRetailer check ', this.allRetailer);
    console.log('all allSubDistributor check ', this.allSubDistributor);
    console.log('all allDistributor check ', this.allDistributor);
    });
    
    }
    allSubDistributorRetailers:any;
    allDistributorRetailers:any[]=[];
    getRetailers(Userid:any,userfirst:any,userlast:any) {
      this.UserfirstName = userfirst;
        this.UserlastName = userlast;
      $('.retailer-Form').hide();
      $('.subDistributor-Form').hide();
      $('.Distributor-Form').hide();
      $('.subDistributorRetailers-Form').show();
      $('.DistributorRetailers-Form').hide();

      const reqMap = {
      // roleMaster: {

      //     id: this.roleMasterId
      // },
      id: Userid,
  
      }
      console.log('sota hu',reqMap);
      // this.selectedDevice = null;
      // console.log('sota ',this.selectedDevice);

      this.HttpService.makeRequestApi('post', 'getAllUserListByCreator', reqMap).subscribe(res => {
      
      this.clientsList = res;
      console.log('khudaa check', this.clientsList);
      this.allSubDistributorRetailers= this.clientsList.content.Active;
      // this.allSubDistributor = this.clientsList.content.SubdistributorList;
      // this.allDistributor = this.clientsList.content.DistributorList;
      console.log('all allRetailer check ', this.allRetailer);
      console.log('all allSubDistributor check ', this.allSubDistributor);
      console.log('all allDistributor check ', this.allDistributor);
      });
      // this.selectedDevice = null;
      }

      parentChange(userID:any,userfirst:any,userlast:any){
        // this.UserfirstName = userfirst;
        // this.UserlastName = userlast;
        const reqMap = {
          "id": this.storageService.get('currentUser').user.id,
          "user":userID,
          "parent":{
            "id":this.selectedDevice
          }
      
          }
          console.log('sota hu',reqMap);
         
    
          this.HttpService.makeRequestApi('post', 'changeParent', reqMap).subscribe(res => {
          
          this.parentChangeList = res;
if(res.code == 206){
  console.log('ParentChange',this.parentChange)
  this.allSubDistributorRetailers=[];
  this.allDistributorRetailers=[];
  this.sweetalert.swalSuccess('Sub-Distributor Changed Successfully')

} else if (res.code != 206){
  this.sweetalert.swalError(res.message)
}
       
          });
      }
      back(){
        this.changeSubDistributor()
      }
      getDetails(Userid:any,userfirst:any,userlast:any){
        this.AdminfirstName = userfirst;
        this.AdminlastName = userlast;
      $('.retailer-Form').hide();
      $('.subDistributor-Form').hide();
      $('.Distributor-Form').hide();
      $('.subDistributorRetailers-Form').hide();
      $('.DistributorRetailers-Form').show();
      const reqMap = {
     
      id: Userid,
  
      }
      console.log('sota hu',reqMap);

      this.HttpService.makeRequestApi('post', 'getAllUserListByCreator', reqMap).subscribe(res => {
      
      this.clientsList = res;
      console.log('khudaa check', this.clientsList);
      this.allDistributorRetailers= this.clientsList.content.Active;

      });
      }
}
