import { Component, OnInit, ViewChild } from '@angular/core';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { HttpClient } from "@angular/common/http";
import { LotteryHttpService } from '../../../services/lottery-http.service';
import * as $ from '../../../../../node_modules/jquery';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-admin-wallet',
  templateUrl: './admin-wallet.component.html',
  styleUrls: ['./admin-wallet.component.scss']
})
export class AdminWalletComponent implements OnInit {
  @ViewChild('walletModal') walletModal: any;
  @ViewChild('modal5') modal5: any;
  options: any;
  transactionCalenderDate:any;
  userOtp: any = {
    pin1: '',
    pin2: '',
    pin3: '',
    pin4: ''
  };
  check = 7;
  pre = 0;
  page = 1;
pageSize = 6;
page2 = 1;
pageSize2 = 6;
page3 = 1;
pageSize3 = 6;
page4 = 1;
pageSize4 = 6;
page5 = 1;
pageSize5 = 6;
  next1 = 0;
  pre1 = true;
  nxt1 = false;
  valueDate: any;
  oneday: any;
  nextday: any;
  today = Date();
  // today: number = Date.now();
  amount;
  balance: any;
  debitBalance: any[] = [];
  creditBalance: any = [];
  allTransactionList: any[] = [];
  Balance: any = {
    "amount": "",
  }
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  id = this.userD.user.roleMaster.id;
  mobile: Number;
  ticket: Number;
  tempMobile: Number;
  tempTicket: Number;
  status: String;
  SubAmount:any;
  allTransactionDate:any;
  WalletBonusDate:any;
  ClaimBonusDate:any;
  DebitDate:any;
  CreditDate:any;
  RequestedDate:any;
  WalletBonusSubAmount:any;
  ClaimBonusSubAmount:any;
  CreditSubAmount:any;
  DebitSubAmount:any;
  RequestedSubAmount:any;
  // url: any = 'http://env-0661550.mj.milesweb.cloud/wallet/';
  userInfo: any = {}
  balanceInfo: any = {}
  userActive:any;
  user: any = {};
  allTransaction: any = [];
  allDebit: any = [];
  allCredit: any = [];
  allBonusWallet: any = [];
  allBonusClaim: any = [];
  transactions: any = [];
  filteredDebitBalance :any[]=[];
  walletBonusBalance: any = [];
  claimBonusBalance: any[] = [];
  navModel2: any = {
    "wallet": 2,
    "walletHistory": 1,
   
  }
  navbar2(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel2.wallet = 2;
      this.navModel2.walletHistory = 1;
    

    }
    else if (input == 2) {
      this.navModel2.wallet = 1;
      this.navModel2.walletHistory = 2;
    }

  }
  constructor(private router: Router, private sweetAlert: SweetAlertService, private storageService: LotteryStorageService,
    private http: HttpClient, private lotteryService: LotteryHttpService,  private datePipe: DatePipe) { }
  clicked() {
    this.tempMobile = this.mobile;
    this.tempTicket = this.ticket;
  }
  navModel: any = {
    "Credit": 1,
    "Debit": 1,
    "ShowTransaction": 2,
    "ShowRequested": 1,
    "BonusClaim": 1,
    "BonusWallet": 1,
  }
  navbar(input: any) {
    console.log(input);
    if (input == 1) {
      this.navModel.Credit = 2;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 1;
      this.navModel.ShowRequested = 1;
      this.navModel.BonusClaim = 1;
      this.navModel.BonusWallet = 1;
    }
    else if (input == 2) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 2;
      this.navModel.ShowTransaction = 1;
      this.navModel.ShowRequested = 1;
      this.navModel.BonusClaim = 1;
      this.navModel.BonusWallet = 1;
    }
    else if (input == 3) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 1;
      this.navModel.ShowRequested = 1;
      this.navModel.BonusClaim = 1;
      this.navModel.BonusWallet = 2;
    } else if (input == 4) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 1;
      this.navModel.ShowRequested = 1;
      this.navModel.BonusClaim = 2;
      this.navModel.BonusWallet = 1;
    }
    else if (input == 5) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 2;
      this.navModel.ShowRequested = 1;
      this.navModel.BonusClaim = 1;
      this.navModel.BonusWallet = 1;
    }
    else if (input == 6) {
      this.navModel.Credit = 1;
      this.navModel.Debit = 1;
      this.navModel.ShowTransaction = 1;
      this.navModel.ShowRequested = 2;
      this.navModel.BonusClaim = 1;
      this.navModel.BonusWallet = 1;
    }
  }
  walletHistory(){
  this.router.navigate(['/client/wallet-Details']);
}
openClientWalletHistory(){
  this.router.navigate(['/client/client-wallet-history']);
}
openDistributorWalletHistory(){
  this.router.navigate(['/distributor/distributor-wallet-History']);

}
  ngOnInit() {
    $('.allTransactionForm').show();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').hide()
    this.walletBalance();
    this.CommissionBalance();
    this.allTransactionDone()
    this.createUserModel();
    this.inActiveUser();
  }
  //url:any = 'http://localhost:8080/'; http://env-0661550.mj.milesweb.cloud/wallet/
 
  inActiveUser(){    
    let reqMap = {      
        id:16
    }
    this.lotteryService.makeRequestApi('post', 'inactive', reqMap).subscribe(res => {
     this.userActive = res;
     console.log('userActive',this.userActive);
    });
  }
  createUserModel() {
    this.userInfo = this.storageService.get('currentUser');
    this.user = {
      "userName": this.userInfo.user.userName,
      "email": this.userInfo.user.email,
      "userId": this.userInfo.user.id
    }
    // this.getBalance();
    // this.getPassbook();
    // this.transactionList;
  }
  // getBalance() {
  //   this.http.post(this.url + 'users/balance', this.user).subscribe(response => {
  //     this.balanceInfo = JSON.parse(JSON.stringify(response));
  //   });
  // }
  // getPassbook() {
  //   this.http.get(this.url + this.user.userId + '/passbook').subscribe(response => {
  //     // this.transactionList = JSON.parse(JSON.stringify(response));
  //   });
  // }
  getTransaction() {
    const reqMap = {
      id: this.storageService.get('currentUser').user.id,

    }
    this.lotteryService.makeRequestApi('post', 'getAllUserListByCreator', {
      id: this.storageService.get('currentUser').user.id,

    }).subscribe(res => {

      this.transactions = res;
      console.log('client check', this.transactions);
      this.allTransaction = this.transactions;
      this.allDebit = this.transactions;
      this.allCredit = this.transactions;
      this.allBonusClaim = this.transactions;
      this.allBonusWallet = this.transactions;
      console.log('all client check ', this.transactions);
    });
  }
  walletBalance() {

    let reqMap = {
      requestBy: {
        id: this.storageService.get('currentUser').user.id,
      }
    }
    this.lotteryService.makeRequestApi('post', 'viewWalletBalance', reqMap).subscribe((res) => {
      this.balance = res;
      console.log(this.balance.content.amount);
      this.Balance.amount = this.balance.content.amount;
    });

  }
  Commissionbalance:any;
  CommissionBalance() {

    let reqMap = {
      user: {
        id: this.storageService.get('currentUser').user.id,
      }
    }
    this.lotteryService.makeRequestApi('post', 'viewCommissionBalance', reqMap).subscribe((res) => {
      this.Commissionbalance = res.content.amount;
     
    });

  }
  requestedList: any = [];

  balanceEntry: any = {
    "firstName": "",
    "status": "",
    "amount": "",
    "createdDate": "",
    "bankName": "",
    "id": ""
  }
  showRequest() {
    $('.allTransactionForm').hide();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').show();
    $('.showClientRequestedForm').hide();

    let reqMap = {
      transactionDate: this.datePipe.transform(this.today, "dd-MM-yyyy"),
      requestBy: {
        id: this.storageService.get('currentUser').user.id,
      }
    }
    this.lotteryService.makeRequestApi('post', 'viewWalletRequest', reqMap).subscribe((res) => {
      this.balance = res.content;

      for (var j = 0; j < this.balance.length; j++) {


        this.requestedList = this.balance[j]
        if (this.requestedList != null) {
          // this.balanceEntry.firstName = this.requestedList.requestTo.firstName;
          // this.balanceEntry.amount = this.requestedList.amount;
          // this.balanceEntry.createDate = this.requestedList.createDate;
          // this.balanceEntry.bankName = this.requestedList.bankName;
          // this.balanceEntry.status = this.requestedList.status.name;
          // this.balanceEntry.id = this.requestedList.id;
        }
      }
      console.log(this.requestedList.id);



    });

  }
  creditList: any = [];
  creditEntry: any = {
    "firstName": "",
    "transactionAmount": "",
    "transactionDate": "",
    "transactionMode": "",


  }
  showCredit() {
    this.CreditDate =  this.datePipe.transform(this.today, "dd-MM-yyyy");

    $('.allTransactionForm').hide();
    $('.creditForm').show();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').hide();
    $('.showClientRequestedForm').hide();

    let reqMap = {
      transactionDate: this.datePipe.transform(this.today, "dd-MM-yyyy"),
      user: {
        id: this.storageService.get('currentUser').user.id
      },
      transactionMode: "Credit"
    }
    this.lotteryService.makeRequestApi('post', 'viewCredit', reqMap).subscribe((res) => {
      this.creditBalance = res.content;
      this.CreditSubAmount = res.codeList;
      // this.CreditDate = res.content[0].transactionDate;

      for (var j = 0; j < this.creditBalance.length; j++) {
        this.creditList = this.creditBalance[j]
      }

    });

  }
  debitList: any = [];
  debitEntry: any = {
    "firstName": "",
    "transactionAmount": "",
    "transactionDate": "",
    "transactionMode": "",
  }
  showDebit() {
    this.DebitDate = this.datePipe.transform(this.today, "dd-MM-yyyy")
    $('.allTransactionForm').hide();
    $('.creditForm').hide();
    $('.debitForm').show();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').hide();
    $('.showClientRequestedForm').hide();

    let reqMap = {
      transactionDate: this.datePipe.transform(this.today, "dd-MM-yyyy"),
      user: {
        id: this.storageService.get('currentUser').user.id
      },
      transactionMode: "Debit"
    }
    this.lotteryService.makeRequestApi('post', 'viewCredit', reqMap).subscribe((res) => {
      this.debitBalance = res.content;
      this.DebitSubAmount = res.codeList;
      // this.DebitDate = res.content[0].transactionDate;
      for (var j = 0; j < this.debitBalance.length; j++) {
        this.debitList = this.debitBalance[j]
        if (this.debitList != null) {
          // this.debitEntry.firstName = this.debitList.user.firstName;
          // this.debitEntry.transactionMode = this.debitList.transactionMode;
          // this.debitEntry.transactionDate = this.debitList.transactionDate;
          // this.debitEntry.transactionAmount = this.debitList.transactionAmount;
        }
      }
    });
  }
  wallletBonusList: any = [];
  wallletBonusEntry: any = {
    "firstName": "",
    "transactionAmount": "",
    "transactionDate": "",
    "transactionType": "",
  }
  walletBonus() {
    this.WalletBonusDate = this.datePipe.transform(this.today, "dd-MM-yyyy"),
    $('.allTransactionForm').hide();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').show();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').hide();
    $('.showClientRequestedForm').hide();

    let reqMap = {
      transactionDate: this.datePipe.transform(this.today, "dd-MM-yyyy"),
      user: {
        id: this.storageService.get('currentUser').user.id
      },
      transactionType: "TicketCommission"
    }
    this.lotteryService.makeRequestApi('post', 'viewWalletBonus', reqMap).subscribe((res) => {
      this.walletBonusBalance = res.content;
      this.WalletBonusSubAmount = res.codeList;
      // this.WalletBonusDate = res.content[0].transactionDate;

      for (var j = 0; j < this.walletBonusBalance.length; j++) {

        this.wallletBonusList = this.walletBonusBalance[j]

        if (this.wallletBonusList != null) {
          console.log(this.wallletBonusList);
          // this.wallletBonusEntry.firstName = this.debitList.user.firstName;
          // this.wallletBonusEntry.transactionType = this.debitList.transactionType;
          // this.wallletBonusEntry.transactionDate = this.debitList.transactionDate;
          // this.wallletBonusEntry.transactionAmount = this.debitList.transactionAmount;
        }
      }
    });
  }

  claimBonusList: any = [];
  claimBonusEntry: any = {
    "firstName": "",
    "transactionAmount": "",
    "transactionDate": "",
    "transactionType": "",
  }
  claimBonus() {
    $('.allTransactionForm').hide();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').show();
    $('.showRequestedForm').hide();
    $('.showClientRequestedForm').hide();

    let reqMap = {
      transactionDate: this.datePipe.transform(this.today, "dd-MM-yyyy"),
      user: {
        id: this.storageService.get('currentUser').user.id
      },
      transactionType: "ClaimBonus"
    }
    this.lotteryService.makeRequestApi('post', 'viewWalletBonus', reqMap).subscribe((res) => {
      this.claimBonusBalance = res.content;
      this.ClaimBonusSubAmount = res.codeList;
      // this.ClaimBonusDate = res.content[0].transactionDate;

      for (var j = 0; j < this.claimBonusBalance.length; j++) {
        this.claimBonusList = this.claimBonusBalance[j]
        if (this.claimBonusList != null) {
          // this.claimBonusEntry.firstName = this.debitList.user.firstName;
          // this.claimBonusEntry.transactionType = this.debitList.transactionType;
          // this.claimBonusEntry.transactionDate = this.debitList.transactionDate;
          // this.claimBonusEntry.transactionAmount = this.debitList.transactionAmount;
        }
      }
    });
  }
  approv(emp, amt) {
    let reqMap = {
      id: emp,
      requestBy: {
        id: this.storageService.get('currentUser').user.id
      }
    }
    console.log(reqMap);
    if (this.Balance.amount < amt) {
      this.sweetAlert.swalError('Balance is Low');
    } else {
      this.lotteryService.makeRequestApi('post', 'approv', reqMap).subscribe((res) => {

      });
      this.sweetAlert.swalSuccess('Claim Approved');
      this.showRequest();
    }

  }

  addMoney() {
    this.isDisabled=true;
    let reqMap = {

      requestBy: {
        id: this.storageService.get('currentUser').user.id
      },
      requestTo: {
        id: this.storageService.get('currentUser').user.createdBy
      },
      // paymentMode: "cash",
      // bankName: "united",
      // remarks: "remarkkk",
      // chequeNo: "2383388383",
      // bankTransactionId: "83kdi3ikdkd",
      // name: "ranjeet",
      amount: this.amount,
      // phoneNo: 438383838
    }
    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'requestWalletAmount', reqMap).subscribe((res) => {
      if (res.code == 206) {
        this.sweetAlert.swalSuccess('Amount Requested');
        this.router.navigate(['/client/wallet-Details']);
        this.walletModal.hide();
        this.walletBalance();
        this.isDisabled=false;
      } else if (res.code == 209) {
        this.sweetAlert.swalError(res.message);

      }
      this.amount = "";
    });

  }
  allTransactionDone() {
    this.allTransactionDate = this.datePipe.transform(this.today, "dd-MM-yyyy");

    $('.allTransactionForm').show();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showRequestedForm').hide();
    $('.showClientRequestedForm').hide();

    // let reqMap = {

    //   user: {
    //     id: this.storageService.get('currentUser').user.id
    //   }
    // }
    // console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'allTransaction',{ transactionDate: this.datePipe.transform(this.today, "dd-MM-yyyy"), 
    user: { id: this.storageService.get('currentUser').user.id } }).subscribe((res) => {
      this.allTransactionList = res.content;
      console.log('Date Kitta hua',this.allTransactionDate);

      this.SubAmount = res.codeList;
      console.log('Paisa Kitta hua',this.SubAmount);
    });
  }
isDisabled = false;
  bookingdate: any;
  slottime: any;
  slots: any;
  ticketDeatils: any; 
  number: any;
  gher: any;
  amount1: any;
  totalprice: any;
  previewModal: any = [];
  ticketModal(userId) {
    let reqMap = {
      bookingDetail: {
        id: userId,
      }
    }

    console.log(reqMap);
    this.lotteryService.makeRequestApi('post', 'bookingId', reqMap).subscribe((res) => {
      if (res.code == 206) {
        this.ticketDeatils = res.content;
        console.log('ticketDeatils', this.ticketDeatils);
        this.previewModal = [];
        this.slots = this.ticketDeatils[0].bookingDetail.slot;
        this.bookingdate = this.ticketDeatils[0].bookingDetail.createDate;
        console.log('slots', this.slots);
        this.slottime = this.slots.drawTimeAm;
        console.log('slotsssssssss', this.slottime);
        this.totalprice = this.ticketDeatils[0].bookingDetail.totalPrice;
        console.log('sdcdsc', this.totalprice);
        for (let demo of this.ticketDeatils) {
          // for(let d of demo.bookingDetail){
          //   this.totalprice=d.totalPrice;
          // }
          this.previewModal.push({
            "gherno": demo.gherno,

            "ticketNo": demo.ticketNo,
            "totalticketAmt": demo.totalticketAmt,
            "numbersSelected": demo.numbersSelected,

          })


        }
        for (let p of this.previewModal) {
          console.log('abc', p.numbersSelected);
          this.bac = p.numbersSelected;

        }


        this.modal5.show();
      }




      console.log('houseno', this.previewModal);
    });
  }
  bac: any;
  backup: any = [];
  requestbalance: any;
  retailerRequestedList() {
    $('.allTransactionForm').hide();
    $('.creditForm').hide();
    $('.debitForm').hide();
    $('.bonusWalletForm').hide();
    $('.bonusClaimForm').hide();
    $('.showClientRequestedForm').show();
    let reqMap = {
      transactionDate: this.datePipe.transform(this.today, "dd-MM-yyyy"),
      requestBy: {
        id: this.storageService.get('currentUser').user.id,
      }
    }
    this.lotteryService.makeRequestApi('post', 'retailerRequested', reqMap).subscribe((res) => {
      this.requestbalance = res.content;
      this.RequestedSubAmount = res.codeList;

      this.RequestedDate = this.datePipe.transform(this.today, "dd-MM-yyyy");


      for (var j = 0; j < this.requestbalance.length; j++) {


        this.requestedList = this.requestbalance[j]
        if (this.requestedList != null) {
          // this.balanceEntry.firstName = this.requestedList.requestTo.firstName;
          // this.balanceEntry.amount = this.requestedList.amount;
          // this.balanceEntry.createDate = this.requestedList.createDate;
          // this.balanceEntry.bankName = this.requestedList.bankName;
          // this.balanceEntry.status = this.requestedList.status.name;
          // this.balanceEntry.id = this.requestedList.id;
        }
      }
      console.log(this.requestedList.id);



    });
  }

  predateFunction() {
    if (this.check == this.pre) {
      this.pre1 = false;
    }
    else {
      this.pre = this.pre + 1;
      this.yesterday(this.pre);
      this.next1 = 0;
    }
    if (this.pre > 0) {
      this.nxt1 = true;
    } else {
      this.nxt1 = false;
    }
  }
  nextdateFunction() {
    if (this.pre > 0) {
      this.nxt1 = true;
      this.next1 = 1;
      this.pre = this.pre - 1;
      this.next(this.next1);
      this.pre1 = true;
    } else {
      this.nxt1 = false;
    }
  }
  dte: any;
  list:any;
  yesterday(value: number) {

    this.dte = new Date();
    this.dte.setDate(this.dte.getDate() - value);
    this.oneday = this.dte.toString();
    console.log(this.oneday);
    this.lotteryService.makeRequestApi('post', 'allTransaction', { transactionDate: this.datePipe.transform(this.dte, "dd-MM-yyyy"), 
     user: { id: this.storageService.get('currentUser').user.id } }).subscribe(
      res => {
        this.allTransactionList = res.content;
                console.log('this.resultList', this.list);
        // this.tempList = res;
        this.today = this.oneday;
        
        // this.default();
      });

  }
  next(value2: number) {
    let one;
    let two;
    one = Date();
    two = Date.now();
    this.dte.setDate(this.dte.getDate() + value2);
    this.nextday = this.dte.toString();
    console.log(this.nextday);
    this.lotteryService.makeRequestApi('post', 'allTransaction', { transactionDate: this.datePipe.transform(this.dte, "dd-MM-yyyy"), 
    user: { id: this.storageService.get('currentUser').user.id } }).subscribe(
      res => {
        this.allTransactionList = res.content;
         console.log('this.resultList', this.list);
        // this.tempList = res;
        this.today = this.dte;
        // this.default();
      });
  }
  
  sl = 0;
  time: any;

getTransactionDetails(){
  console.log(this.transactionCalenderDate);
  let reqMap = {
    transactionDate :this.datePipe.transform(this.transactionCalenderDate, "dd-MM-yyyy") ,

    user: {
      id: this.storageService.get('currentUser').user.id,
    },
  }
  console.log(reqMap);
  this.lotteryService.makeRequestApi('post', 'allTransaction',reqMap).subscribe((res) => {
    this.allTransactionList = res.content;
    this.allTransactionDate = res.content[0].transactionDate;
    this.SubAmount = res.codeList;
    console.log(this.allTransactionList);
  });

}
transactionCreditDate:any;
transactionDebitDate:any;
transactionBonusWalletDate:any;
transactionBonusClaimDate:any;
transactionRequestedDate:any;
getCreditDetails(){
  let reqMap = {
    transactionDate :this.datePipe.transform(this.transactionCreditDate, "dd-MM-yyyy") ,

    user: {
      id: this.storageService.get('currentUser').user.id,
    },
    transactionMode: "Credit"
  }
  console.log(reqMap);
  this.lotteryService.makeRequestApi('post', 'viewCredit',reqMap).subscribe((res) => {
    this.creditBalance = res.content;
    this.CreditSubAmount = res.codeList;
    this.CreditDate = res.content[0].transactionDate;

        console.log(this.allTransactionList);
  });
}
getDebitDetails(){
  let reqMap = {
    transactionDate :this.datePipe.transform(this.transactionDebitDate, "dd-MM-yyyy") ,

    user: {
      id: this.storageService.get('currentUser').user.id,
    },
    transactionMode: "Debit"
  }
  console.log(reqMap);
  this.lotteryService.makeRequestApi('post', 'viewCredit',reqMap).subscribe((res) => {
    this.debitBalance = res.content;
    this.filteredDebitBalance = this.debitBalance;
    this.DebitSubAmount = res.codeList;
    this.DebitDate = res.content[0].transactionDate;

        console.log(this.allTransactionList);
  });
}
getBonusWalletDetails(){
  let reqMap = {
    transactionDate :this.datePipe.transform(this.transactionBonusWalletDate, "dd-MM-yyyy") ,

    user: {
      id: this.storageService.get('currentUser').user.id,
    },
    transactionType: "TicketCommission"
  }
  console.log(reqMap);
  this.lotteryService.makeRequestApi('post', 'viewWalletBonus',reqMap).subscribe((res) => {
    this.walletBonusBalance = res.content;
    this.WalletBonusSubAmount = res.codeList;
    this.WalletBonusDate = res.content[0].transactionDate;

        console.log(this.allTransactionList);
  });
}
getBonusClaimDetails(){
  let reqMap = {
    transactionDate :this.datePipe.transform(this.transactionBonusClaimDate, "dd-MM-yyyy") ,

    user: {
      id: this.storageService.get('currentUser').user.id,
    },
    transactionType: "ClaimBonus"
  }
  console.log(reqMap);
  this.lotteryService.makeRequestApi('post', 'viewWalletBonus',reqMap).subscribe((res) => {
    this.claimBonusBalance = res.content;
    this.ClaimBonusSubAmount = res.codeList;
    this.ClaimBonusDate = res.content[0].transactionDate;

    console.log(this.allTransactionList);
  });
}
getRequestedDetails(){
  this.RequestedDate = this.transactionRequestedDate;

  let reqMap = {
    transactionDate :this.datePipe.transform(this.RequestedDate, "dd-MM-yyyy") ,
    requestBy: {
      id: this.storageService.get('currentUser').user.id,
    }
  }
  console.log(reqMap);
  this.lotteryService.makeRequestApi('post', 'retailerRequested',reqMap).subscribe((res) => {
    this.requestbalance = res.content;
    this.RequestedSubAmount = res.codeList;
    this.RequestedDate = this.transactionRequestedDate;


    console.log(this.requestbalance);
  });
}

}
