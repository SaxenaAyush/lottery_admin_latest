import { Component, OnInit } from '@angular/core';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-transaction-details',
  templateUrl: './view-transaction-details.component.html',
  styleUrls: ['./view-transaction-details.component.scss']
})
export class ViewTransactionDetailsComponent implements OnInit {
  viewdeepClientDetails:any[]=[];
  viewdeepClientDetailsList:any[]=[];
  viewdeepClientDetailsDate:any;
  firstName:any;
  lastName:any;
  date:any;
  Code:any;
  page = 1;
pageSize = 8;
  constructor(private alertService: SweetAlertService, private datePipe: DatePipe, 
    private lotteryHttpService: LotteryHttpService, private route: Router,
     private storageService: LotteryStorageService) { }

  ngOnInit() {
   this.transactionDetails();
  }
transactionDetails(){
  this.viewdeepClientDetails = this.storageService.get('deepDetails');
this.viewdeepClientDetailsList = this.viewdeepClientDetails;
this.firstName = this.storageService.get('first');
this.lastName = this.storageService.get('last');
this.date = this.storageService.get('date');
this.Code = this.storageService.get('code');

console.log('viewdeepClientDetailsList',this.viewdeepClientDetailsList);
}


  back(){
this.route.navigate(['/distributor/distributor-report']);
  }
  // slotDetails(){

  // }
  slotTime:any
  slotDetails(slotId: any,Date:any,drawTimeAm:any,retailer:any) {
  this.slotTime = drawTimeAm
  let slotid : number =slotId;
    this.lotteryHttpService.makeRequestApi('post', 'slotDetails', {
     
      retailerId: retailer,
      slot: {
        id : slotid
      },
      bookingDate : Date,
  
    }).subscribe(res => {
    console.log('hello user data', res);
    this.storageService.set('slotsData', res.content);
    this.storageService.set('id', slotId);
    this.storageService.set('drawTime', this.slotTime);
    this.storageService.set('date', res.content.bookingDetail[0].bookingDate);
    this.route.navigate(['/distributor/slot-report']);
    });
    
}
}
