import { Component, OnInit } from '@angular/core';
import { LotteryStorageService } from '../../../services/lottery-storage.service';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import * as $ from '../../../../../node_modules/jquery';
import {FileUploader} from 'ng2-file-upload';

@Component({
  selector: 'app-outscreen-result',
  templateUrl: './outscreen-result.component.html',
  styleUrls: ['./outscreen-result.component.scss']
})
export class OutscreenResultComponent implements OnInit {
  public uploader: FileUploader = new FileUploader({});
  imageUrl: string | ArrayBuffer;
  imagePK:any;
  loggedInUserDetails: any;
  constructor(private lotteryService: LotteryHttpService,private router: Router,
    private storageService: LotteryStorageService, private datePipe: DatePipe) {
  }
  check = 7;
  pre = 0;
  next1 = 0;
  pre1 = true;
  nxt1 = false;
  valueDate: any;
  today = Date();
  tempdate = this.today;
  checkdate = this.today.toString();
  bookingslot: any = [];
  tempbookingslot: any = [];
  resultList: any = [];
  newList: any = [];
  tempList: any = [];
  num: String;
  newNum: string;
  oneday: any;
  nextday: any;
  isExist: Boolean;
  searchText: any;
  userD = this.storageService.get('currentUser');
  userName = this.userD.user.userName;
  phoneNumber = this.userD.user.phoneNumber;
  phoneDisp = "(" + this.phoneNumber + ")";
  role = this.userD.user.roleMaster.name;
  ngOnInit() {
    this.loggedInUserDetails = this.storageService.get('imageData');
    if (this.loggedInUserDetails.eoImage !== null) {
      this.imageUrl = this.loggedInUserDetails.eoImage.imageUrl;
      console.log('Images data',this.loggedInUserDetails);
    }
    $(document).on('click', '.btn-group btn2', function (e) {
      $(".active").not($(this).addClass('active')).removeClass();
  });
    this.getSlotList();
    // this.default();
    setInterval(() => {
      this.default();
    }, 1000);
    console.log('this.today', this.today);
    console.log('this.checkdate', this.checkdate);
    console.log('this.tempdate', this.tempdate);
  }

  flag = 0;
  slots: any;
  tempSlots: any = [];
  status = 0;
  lunchbook() {
    let three;
    let two;
    two = "14:00";
    three = "15:00";

    for (let book of this.slots) {
      if (book.resultStartTime >= two && book.endTime <= three) {

      } else {
        this.tempSlots.push(book)
        this.status == 1
      }
    }

    console.log('bookingslot slot ali', this.bookingslot);
  }
  getSlotList() {
    this.lotteryService.makeRequestApi('get', 'slotlist').subscribe(slot => {
      if (slot != null) {
        this.bookingslot = slot;
        this.slots = slot;
        this.lunchbook();
        // for (let slot of this.bookingslot) {
        //   this.getResult(slot.id);
        // }
        //   for (let book of this.slots) {

        // if()
        //     this.tempSlots.push(book)
        //     this.status == 1

        //   }
        console.log('asaaaaaa', this.tempSlots);
      }
      else {
        console.log('error');
      }

    });
  }

  predateFunction() {
    if (this.check == this.pre) {
      this.pre1 = false;
    }
    else {
      this.pre = this.pre + 1;
      this.yesterday(this.pre);
      this.next1 = 0;
    }
    if (this.pre > 0) {
      this.nxt1 = true;
    } else {
      this.nxt1 = false;
    }
  }
  nextdateFunction() {
    if (this.pre > 0) {
      this.nxt1 = true;
      this.next1 = 1;
      this.pre = this.pre - 1;
      this.next(this.next1);
      this.pre1 = true;
    } else {
      this.nxt1 = false;
    }
  }
  dte: any;
  isDisabled = false;
  isDisabled1 = false;
  yesterday(value: number) {

    this.dte = new Date();
    this.dte.setDate(this.dte.getDate() - value);
    this.oneday = this.dte.toString();
    console.log(this.oneday);
    this.lotteryService.makeRequestApi('post', 'viewResults', { bookingDate: this.datePipe.transform(this.dte, "dd-MM-yyyy"), slotMaster: { id: 1 } }).subscribe(
      res => {
        this.resultList = res;
        // console.log('this.resultList', this.resultList);
        this.tempList = res;
        this.today = this.oneday;
        // console.log('this.today', this.today);
        // console.log('this.checkdate prev', this.checkdate);
        // console.log('this.tempdate', this.tempdate);

        this.default();
      });

  }
  next(value2: number) {
    let one;
    let two;
    one = Date();
    two = Date.now();
    this.dte.setDate(this.dte.getDate() + value2);
    this.nextday = this.dte.toString();
    console.log(this.nextday);
    this.lotteryService.makeRequestApi('post', 'viewResults', { bookingDate: this.datePipe.transform(this.dte, "dd-MM-yyyy"), slotMaster: { id: 1 } }).subscribe(
      res => {
        this.resultList = res;
        // console.log('this.resultList', this.resultList);
        this.tempList = res;
        this.today = this.dte;
        this.default();
      });
  }
  // todayResult(slotId: any) {
  //   this.flag = 0;

  //   this.lotteryService.makeRequestApi('post', 'viewResults', { bookingDate: this.datePipe.transform(new Date(), "dd-MM-yyyy"), slotMaster: { id: 1 } }).subscribe(
  //     res => {
  //       this.resultList = res;
  //       this.tempList = res;

  //     });


  // }
  sl = 0;
  time: any;
  getslot() {
    var d = new Date();
    let h: any;
    let m: any;
    let s: any;
    h = d.getHours();
    m = d.getMinutes();
    s = d.getSeconds();

    let min;
    if(m<10){
      min="0"+m;
    }else{
      min=m;
    }
    this.match = h + ":" + min;
    // console.log(this.match)
    for (let book of this.bookingslot) {
      if (this.sl == 0) {
        if (this.match > book.endTime) {
          this.tempbookingslot.push(book)

        }
      }

      if (this.match <= book.startTime) {
        this.sl = 1;
      }

    }
    //  for(let u of this.tempbookingslot){

    //   this.time = u.drawTimeAm;
    //  }
    // this.time = this.tempbookingslot.slot.drawTimeAm;


  }
  match: any;
  matchtime: any;
  matchEndtime: any;
  default() {
    // console.log('hi');
    var d = new Date();
    let h: any;
    let m: any;
    let s: any;
    h = d.getHours();
    m = d.getMinutes();
    s = d.getSeconds();
    let min;
    if(m<10){
      min="0"+m;
    }else{
      min=m;
    }
    this.match = h + ":" + min;
    let pre;
    for (let book of this.bookingslot) {
      pre = book;
      if (this.match >= book.resultStartTime && this.match <= book.endTime) {
        this.matchtime = book.resultStartTime;

        this.matchEndtime = book.drawTime;
        // console.log('pre..', pre);
        this.getResult(pre.id - 1);


      }
    }
    this.getslot();
  }
  slotflag = 0;
  getResult(slotId: any) {
  
    // console.log('chicken dinner1');
    if (this.slotflag == 0) {
      this.slotTime3(slotId);
      // console.log('chicken dinner2');
      this.oneday = this.today.toString();
      // console.log('chicken dinner', this.oneday);
      this.lotteryService.makeRequestApi('post', 'viewResults', { bookingDate: this.datePipe.transform(this.today, "dd-MM-yyyy"), slotMaster: { id: slotId } }).subscribe(
        res => {
          this.resultList = res;
          this.tempList = res;
          for (let bt of this.tempSlots) {
            if (bt.id == slotId) {
              bt.status = 2;
            } else {
              bt.status = 1;
  
            }
          }
          // console.log('this.resultList', this.resultList);
          // console.log('chicken dinner', this.today);
        });
    }
  }


  getmyResult(slotId: any) {
    this.isDisabled = true;
    this.isDisabled1 = true;
    this.slotTime3(slotId)
    this.slotflag = 1
  
    this.oneday = this.today.toString();
    // console.log('chicken dinner', this.oneday);
    this.lotteryService.makeRequestApi('post', 'viewResults', { bookingDate: this.datePipe.transform(this.today, "dd-MM-yyyy"), slotMaster: { id: slotId } }).subscribe(
      res => {
        this.resultList = res;
        if(this.resultList == null){
          this.isDisabled = false;
          this.isDisabled1 = false;
        }else{
          this.tempList = res;
          this.isDisabled = false;
          this.isDisabled1 = false;
        }
       
        // console.log('this.resultList', this.resultList);
        console.log('chicken dinner', this.tempList);
        for (let bt of this.tempSlots) {
          if (bt.id == slotId) {
            bt.status = 2;
          } else {
            bt.status = 1;

          }
        }
        this.storageService.set('resultpreview', this.resultList);

      });


  }
  printResult(){
    this.router.navigate(['/result-print']);

  }
  slotTime3(id: any) {
    for (let demo of this.bookingslot) {
      if (demo.id == id) {
        this.time = demo.drawTimeAm;
      }
    }
  }
  search1() {
    console.log(this.searchText);
    for (let item of this.tempList) {
      if (this.searchText == item.gherNo) {
        this.resultList = [];
        this.resultList.push(item)

        this.searchText = "";
      }
      // console.log('this.resultList', this.resultList);

    }
  }


}
