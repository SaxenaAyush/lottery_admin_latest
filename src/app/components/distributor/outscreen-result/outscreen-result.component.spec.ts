import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutscreenResultComponent } from './outscreen-result.component';

describe('OutscreenResultComponent', () => {
  let component: OutscreenResultComponent;
  let fixture: ComponentFixture<OutscreenResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutscreenResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutscreenResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
