import { Component, OnInit } from '@angular/core';
import { SweetAlertService } from '../../../common/sharaed/sweetalert2.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { LotteryHttpService } from '../../../services/lottery-http.service';
import { LotteryStorageService } from '../../../services/lottery-storage.service';

@Component({
  selector: 'app-retailer-wallet-details',
  templateUrl: './retailer-wallet-details.component.html',
  styleUrls: ['./retailer-wallet-details.component.scss']
})
export class RetailerWalletDetailsComponent implements OnInit {
  viewdeepClientDetails:any[]=[];
  viewdeepClientDetailsList:any[]=[];
  viewdeepClientDetailsDate:any;
  firstName:any;
  lastName:any;
  date:any;
  Code:any;
  id:any;
  page = 1;
  pageSize = 8;
  constructor(private alertService: SweetAlertService, private datePipe: DatePipe, 
    private lotteryHttpService: LotteryHttpService, private route: Router,
     private storageService: LotteryStorageService) { }

  ngOnInit() {
   this.wallettransactionDetails();
  }
wallettransactionDetails(){
  this.viewdeepClientDetails = this.storageService.get('deepDetails');
this.viewdeepClientDetailsList = this.viewdeepClientDetails;
this.firstName = this.storageService.get('first');
this.lastName = this.storageService.get('last');
this.date = this.storageService.get('date');
this.Code = this.storageService.get('code');
this.id = this.storageService.get('empid');
console.log('Ea ID hai', this.id)
console.log('viewdeepClientDetailsList',this.viewdeepClientDetailsList);
}


  back(){
this.route.navigate(['/distributor/wallet-History']);
  }
  slotDetails(){
    this.route.navigate(['/distributor/slot-report']);

  }
  WalletTodayDate:any;
  walletReportByDate(){

    this.lotteryHttpService.makeRequestApi('post', 'retailerWalletHistory', {
      transactionDate :this.datePipe.transform(this.WalletTodayDate, "dd-MM-yyyy"),
      user: {
        id: this.id
      }
      }).subscribe(res => {
      console.log('hello user data', res);
      this.viewdeepClientDetailsList =  res.content     
      });
      this.date = this.datePipe.transform(this.WalletTodayDate, "dd-MM-yyyy")
  }
}
