import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerWalletDetailsComponent } from './retailer-wallet-details.component';

describe('RetailerWalletDetailsComponent', () => {
  let component: RetailerWalletDetailsComponent;
  let fixture: ComponentFixture<RetailerWalletDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerWalletDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerWalletDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
