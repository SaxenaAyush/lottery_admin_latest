import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SweetAlertService} from './../../common/sharaed/sweetalert2.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TwoDecimalFormatNumberPipe } from '../../pipes/two-decimal-format-number.pipe';
import { FileUploadModule } from 'ng2-file-upload';
// import { PdfmakeModule } from '../../../../node_modules/ng-pdf-make';
// import { Observable } from 'rxjs';


@NgModule({
  imports: [CommonModule, FileUploadModule],
  declarations: [ TwoDecimalFormatNumberPipe],
  providers: [SweetAlertService],
  exports: [ NgbModule, TwoDecimalFormatNumberPipe,FileUploadModule
    ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [SweetAlertService]
    };
  }
}
