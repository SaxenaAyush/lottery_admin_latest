import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LotteryHttpService} from '../../services/lottery-http.service';
import {FileUploader} from 'ng2-file-upload';
const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  imagePK: any;
  public uploader: FileUploader = new FileUploader({url: URL});

  @Output() addImageObject = new EventEmitter();
  @Input() fileLocationIndex: any;
  @Input() fileUrl: any;

  constructor(private dataService: LotteryHttpService) {
    const extract = function (dataURI) {
      const binary = atob(dataURI.split(',')[1]);
      const array = [];
      for (let i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
      }
      return array;
    };
    this.uploader.onAfterAddingFile = (file) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        this.fileUrl = reader.result;
        const imgData = {
          'detail': this.fileUrl,
          'entityName': 'EOLocation',
          'className': 'EOImage',
          'type': file.file.type
        };
        this.dataService.makeRequestApi('post', 'createImgObject', imgData).subscribe((res: any) => {
          this.imagePK = res.data.primaryKey;
          const sentVAL = {
            'loc': this.fileLocationIndex,
            'pk': this.imagePK
          };
          this.addImageObject.emit(sentVAL);
        }, err => {
          let res = {
            'data': {
              'primaryKey': 21,
              'entityName': null,
              'displayName': null,
              'imageUrl': null,
              'type': null,
              'headerPk': 0,
              'saveNo': 0,
              'imageStorePath': ''
            }
          }
          this.imagePK = res.data.primaryKey;
          const sentVAL = {
            'loc': this.fileLocationIndex,
            'pk': this.imagePK
          };
          this.addImageObject.emit(sentVAL);
        });
      };
      console.log(reader.readAsDataURL(file._file));
    };
  }

  fileSelected(event) {
  }

  ngOnInit() {
  }

}
