import 'reflect-metadata';
import '../polyfills';
import { Component } from '@angular/core';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ModalComponent } from './modal.component';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ElectronService } from './providers/electron.service';
import { TallyConnectorService } from './providers/tallyconnector.service';
import { WebviewDirective } from './directives/webview.directive';
import { AppComponent } from './app.component';
import { ViewClientComponent } from './components/distributor/view-client/view-client.component';
import { DashboardComponent } from './components/distributor/dashboard/dashboard.component';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { LoginComponent } from './components/login/login.component';
import { SharedModule } from './common/sharaed/sharaed.module';
import { HttpModule } from '@angular/http';
import { ViewClientDetailsComponent } from './components/distributor/view-client-details/view-client-details.component';
import { AddNewClientComponent } from './components/distributor/add-new-client/add-new-client.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { AdminProfileComponent } from './components/distributor/admin-profile/admin-profile.component';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { QRCodeModule } from 'angularx-qrcode';
import { DatePipe } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { MatIconModule } from '@angular/material';
import { AsideComponent } from './components/aside/aside.component';
import { ViewDeactivatedclientComponent } from './components/distributor/view-Deactivatedclient/view-Deactivatedclient.component';
import { AboutUsComponent } from './components/aboutUs/aboutUs.component';
import { CertificateComponent } from './components/certificate/certificate.component';
import { TermsComponent } from './components/login/terms-modal.component';
import { from } from 'rxjs';
const { remote } = require('electron');
import { WalletModalComponent } from './components/distributor/admin-wallet/wallet-modal.component';
import { TicketpreviewComponent } from './ticketpreview/ticketpreview.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import { ShowErrorsComponent } from './validator/show-error';
import { ReprintticketComponent } from './reprintticket/reprintticket.component';
import {NgxChildProcessModule} from 'ngx-childprocess';
import {NgxFsModule} from 'ngx-fs';
import { PrintresultComponent } from './printresult/printresult.component';
import {HotkeyModule} from 'angular2-hotkeys';
import { ReportPrintComponent } from './report-print/report-print.component';
import { CalimReprintticketComponent } from './claim-reprint/claim-reprintticket.component';
import { DistributorReportComponent } from './components/distributor/distributor-report/distributor-report.component';
import { ViewTransactionDetailsComponent } from './components/distributor/view-transaction-details/view-transaction-details.component';
import { SlotsReportsComponent } from './components/distributor/slots-reports/slots-reports.component';
import { WalletHistoryComponent } from './components/distributor/wallet-history/wallet-history.component';
import { DistributorDailyReportComponent } from './components/distributor/daily-report/distributor-daily-report.component';
import { DistributorSelfReportComponent } from './components/distributor/distributor-self-report/distributor-self-report.component';
import { RetailerWalletDetailsComponent } from './components/distributor/retailer-wallet-details/retailer-wallet-details.component';
import { PrintClaimVsBookingComponent } from './print-claim-vs-booking/print-claim-vs-booking.component';
import { FirstClaimBookingPrintComponent } from './first-claim-booking-print/first-claim-booking-print.component';
import { DurationReportPrintComponent } from './duration-report-print/duration-report-print.component';
import { AdminWalletComponent } from './components/distributor/admin-wallet/admin-wallet.component';
import { ResultComponent } from './components/distributor/result/result.component';
import { UserProfileComponent } from './components/distributor/user-profile/user-profile.component';
import { SettingComponent } from './components/distributor/setting/setting.component';
import { ChangeParentComponent } from './components/distributor/change-parent/change-parent.component';
import { AdvertisementComponent } from './components/distributor/advertisement/advertisement.component';
import { OutscreenResultComponent } from './components/distributor/outscreen-result/outscreen-result.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}



@NgModule({

  declarations: [
    AppComponent,
    ShowErrorsComponent,
    AsideComponent,   
    WebviewDirective,
    ModalComponent,
    TermsComponent,   
    ViewClientComponent,
    ViewDeactivatedclientComponent,
    DashboardComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    LoginComponent,
    WalletModalComponent,
    ViewClientDetailsComponent,
    AddNewClientComponent,  
    AdminProfileComponent,
    HeaderComponent, 
    AboutUsComponent,
    CertificateComponent,
    TicketpreviewComponent,
    ShowErrorsComponent,
    ReprintticketComponent,
    PrintresultComponent,  
    ReportPrintComponent,
    CalimReprintticketComponent,
    DistributorReportComponent,
    ViewTransactionDetailsComponent,
    SlotsReportsComponent,
    WalletHistoryComponent,
    DistributorDailyReportComponent,
    DistributorSelfReportComponent,
    RetailerWalletDetailsComponent,
    PrintClaimVsBookingComponent,
    FirstClaimBookingPrintComponent,
    DurationReportPrintComponent,
    AdminWalletComponent,
    ResultComponent,
    UserProfileComponent,
    SettingComponent,
    ChangeParentComponent,
    AdvertisementComponent,
    OutscreenResultComponent
  ],
  imports: [
    SharedModule.forRoot(),
    BrowserModule,
    FormsModule,    
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule,
    NgxSelectModule,
    NgxQRCodeModule,
    QRCodeModule,
    NgxBarcodeModule,
    NgxChildProcessModule,
    NgxFsModule,
    HotkeyModule.forRoot(),
    NgxDaterangepickerMd.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [ElectronService, TallyConnectorService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {
}