import { environment } from '../../environments/environment';

export class ConstantsHelper {


public static readonly getAPIUrl = {

addClient: () => `${environment.baseUrl}/signup`,
userLogin: () => `${environment.baseUrl}/alldetailsignin`,
booking: () => `${environment.baseUrl}/book-ticket`,
kycDetails: () => `${environment.baseUrl}/add-kyc-detail`,
personalDetail: () => `${environment.baseUrl}/add-personal-detail`,
bankDetails: () => `${environment.baseUrl}/add-bank-detail`,
getRoleList: () => `${environment.baseUrl}/get-roles`,
getPassbookDetails: () => `${environment.baseUrl}/wallet/users/1/passbook`,
slotlist: () => `${environment.baseUrl}/get-slot-list`,
priceList: () => `${environment.baseUrl}/get-ticket-price`,
// userDetails: (id: number) => `${environment.baseUrl}//get-employeeById/`+id,
// getResults: (bookingDate= "25-05-2019",id= 1) => `${environment.baseUrl}//get-employeeById/`+bookingDate+'/' +id,
viewResults: () => `${environment.baseUrl}/get-result`,
clientDetails: () => `${environment.baseUrl}/get-employee`,
getUserByCreator: () => `${environment.baseUrl}/getUserListByCreator`,
getAllUserListByCreator: () => `${environment.baseUrl}/getAllUserListByCreator`,
deleteClient: () => `${environment.baseUrl}/getChangeStatusOfUser`,
reset: () => `${environment.baseUrl}/resetPassword`,
changePassword: () => `${environment.baseUrl}/change-password`,
viewClient: () => `${environment.baseUrl}/userDetailSignin`,
deductBalance: () => `${environment.baseUrl}/deduct-amount-bycreator`,
// licenseRenewal: () => `${environment.baseUrl}//licence-renewal`,
// resetMachine: () => `${environment.baseUrl}//reset-machinekey`,
settledCliam: () => `${environment.baseUrl}/settled-claim`,
searchByNumber: () => `${environment.baseUrl}/find-my-number1`,
farwordClaim: () => `${environment.baseUrl}/request-claim`,
viewRequestClaim: () => `${environment.baseUrl}/view-requested-claim`,
viewDispatchClaim: () => `${environment.baseUrl}/view-dispatch-claim`,
viewAllTransactionClaim: () => `${environment.baseUrl}/view-claimed-history`,
viewWalletBalance: () => `${environment.baseUrl}/view-wallet-balance`,
viewCommissionBalance: () => `${environment.baseUrl}/get-bonus-balance`,
viewWalletRequest: () => `${environment.baseUrl}/view-wallet-request`,
// viewDebit: () => `${environment.baseUrl}//view-debit`,
viewCredit: () => `${environment.baseUrl}/view-debit-credit-datewise`,
viewWalletBonus: () => `${environment.baseUrl}/view-wallet-claim-bonus-datewise`,
approv: () => `${environment.baseUrl}/approve-wallet-request`,
requestWalletAmount: () => `${environment.baseUrl}/request-wallet-amount`,
allTransaction: () => `${environment.baseUrl}/view-wallet-transaction-datewise`,
getAmount: () => `${environment.baseUrl}/view-wallet-balance`,
getAllResult: () => `${environment.baseUrl}/get-result-bydate`,
commission: () => `${environment.baseUrl}/view-wallet-commission`,
saveCommission: () => `${environment.baseUrl}/save-single-commission`,
updateEmployee: () => `${environment.baseUrl}/update-employee`,
ticketHistory: () => `${environment.baseUrl}/ticket-by-user`,
ticketHistoryByDate: () => `${environment.baseUrl}/ticket-by-userIdAndDate`,
cancelHistoryByDate: () => `${environment.baseUrl}/cancle-history-bydate`,
bookingvsClaim: () => `${environment.baseUrl}/get_ticketbooking_claim_bydate`,
allUserCreatedBy: () => `${environment.baseUrl}/allUserCreatedBy`,
allUserCreatedById: () => `${environment.baseUrl}/get_ticketbooking_claim_bydate_byRetailerID`,
walletHistory: () => `${environment.baseUrl}/allUserRechargeDetails`,
retailerWalletHistory: () => `${environment.baseUrl}//RetailerWalletDetails`,
slotDetails: () => `${environment.baseUrl}//get_ticketbooking_claim_bydate_byRetailerID_slotID`,
reportDatewise: () => `${environment.baseUrl}/retailer-report-datewise`,
toFromDateReport: () => `${environment.baseUrl}//get_ticketbooking_claim_byTwodate_byRetailerID`,
loginId: () => `${environment.baseUrl}/alldetailsuser`,
bookingId: () => `${environment.baseUrl}/ticket-by-bookingid`,
ticketCancle: () => `${environment.baseUrl}/cancle-ticket`,
transferAmount: () => `${environment.baseUrl}/transfer-amount`,
retailerRequested: () => `${environment.baseUrl}/view-my-wallet-request-datewise`,
Transfer: () => `${environment.baseUrl}/approve-direct-wallet-request`,
getLicense: () => `${environment.baseUrl}/get-active-licence`,
licenceRenewal:() => `${environment.baseUrl}/licence-renewal`,
support:() => `${environment.baseUrl}/supports`,
resetMachine:() => `${environment.baseUrl}/reset-machinekey`,
dailyReport:() => `${environment.baseUrl}/retailer-report`,
inactive:() => `${environment.baseUrl}/active-inactive-user`,
// userDetails: (id: number) => `${environment.baseUrl}//get-employeeById/`+id,
// getResults: (bookingDate= "25-05-2019",id= 1) => `${environment.baseUrl}//get-employeeById/`+bookingDate+'/' +id,

http://localhost:8080/find-my-number

{
"id": 160
}
};

}