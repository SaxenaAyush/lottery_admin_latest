import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintresultComponent } from './printresult.component';

describe('PrintresultComponent', () => {
  let component: PrintresultComponent;
  let fixture: ComponentFixture<PrintresultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintresultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
