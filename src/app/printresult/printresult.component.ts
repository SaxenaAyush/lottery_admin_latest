import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LotteryStorageService } from '../services/lottery-storage.service';
import { FsService } from 'ngx-fs';
import { ChildProcessService } from 'ngx-childprocess';

@Component({
  selector: 'app-printresult',
  templateUrl: './printresult.component.html',
  styleUrls: ['./printresult.component.scss']
})
export class PrintresultComponent implements OnInit {
  resultPrint: any ;
  resultcount: any = [];
  bookingDate: any;
  result:any;
  slotTime:any;
  constructor(private router: Router, private storageService: LotteryStorageService, private _fsService: FsService,
    private childProcess: ChildProcessService) {

  }

  ngOnInit() {
    this.resultPrint = this.storageService.get('resultpreview');
    this.result = this.resultPrint
    console.log('resultpreview', this.result);
    this.bookingDate = this.resultPrint[0].bookingDate
    this.slotTime = this.resultPrint[0].slotMaster.drawTimeAm

    setTimeout(() => {
      console.log('Test');
      window.print();
      this.back();
  }, 1000/60);  
  }
  new() {
    for (let data of this.resultPrint) {

      this.resultcount.push({
        "slot": data.bookingDetails.slot.drawTimeAm,
      });

    }
  }
  back(){
    this.router.navigate(['/client/result-view'])
  }

  
  }
