
import { Component, OnInit } from '@angular/core';
import getMAC, { isMAC } from 'getmac'

// import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LotteryStorageService } from '../services/lottery-storage.service';
import { ViewChild } from '@angular/core';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import * as jspdf from '../../../node_modules/jspdf/dist/jspdf.node.min.js';
import * as $ from '../../../node_modules/jquery';
import { ElementRef } from '@angular/core';
import { ChildProcessService } from 'ngx-childprocess';
import { FsService } from 'ngx-fs';

(window as any).html2canvas = html2canvas;

// import { PrintService, UsbDriver, WebPrintDriver } from 'ng-thermal-print';
// import { PrintDriver } from 'ng-thermal-print/lib/drivers/PrintDriver';
@Component({
  selector: 'app-ticketpreview',
  templateUrl: './ticketpreview.component.html',
  styleUrls: ['./ticketpreview.component.scss']
})
export class TicketpreviewComponent implements OnInit {
  // status: boolean = true;
  // usbPrintDriver: UsbDriver;
  // webPrintDriver: WebPrintDriver;
  // ip: string = '';
  @ViewChild('modal2') modal2: any;
  @ViewChild('content') content: ElementRef;
  constructor(private router: Router, private storageService: LotteryStorageService, private _fsService: FsService, private childProcess: ChildProcessService) {

  }


  singlePrice = this.storageService.get('singleprice');
  userD = this.storageService.get('currentUser');
  retailerID = this.userD.user.userCode;
  billModel: any = [];
  billModel1: any = [];
  slots: any = [];
  totalModel: any = {
    "total": 0
  }
  today: any;
  ngOnInit() {
    this.qtyno();
    console.log(getMAC());
    setInterval(() => {
      this.today = Date.now()
    }, 1000);
    this.billModel1 = this.storageService.get('preview');
    console.log('bill model', this.billModel1);

    this.billModel = this.billModel1.content;

    this.sepratedata();

    //this.modal2.show();
    // window.print();
    //this.router.navigate(['/client/booking']);

  }
  bill = {

    "ticketid": 324,
    "values ": {
      "slots": "slots",
      "bookingDetails": "bookingDetail",
      "ticketNumbers": "ticketNumbers"
    }
  }
  totaltickets: any = [];
  ticketcount: any = [];

  sepratedata() {
    for (let bill of this.billModel) {
      // for(let bl of bill){
      //   for(let b1 of bl.bookingDetails){
      this.ticketcount.push({
        "id": bill.bookingDetails.id,
        "ticketId": parseInt(bill.bookingDetails.ticketId)
      });

      this.totaltickets.push({
        "id": bill.bookingDetails.id,
        "retailerId": bill.bookingDetails.retailerId,
        "bookingDate": bill.bookingDetails.bookingDate,
        "createDate": bill.bookingDetails.createDate,
        "totalPrice": bill.bookingDetails.totalPrice,
        "slot": bill.bookingDetails.slot.drawTimeAm,
        "totalTicket": bill.bookingDetails.totalTicket,
        "ticketNumbers": bill.ticketNumbers,
        "ticketId": parseInt(bill.bookingDetails.ticketId)
      })
      //   }
      // }

    }
    console.log('totaltickets', this.totaltickets);
    console.log('ticketcount', this.ticketcount);
    // setTimeout(function() {  this.captureScreen(); }, 5000);
    this.captureScreen(this.totaltickets[0].ticketId);
  }

  //  print3(){
  //   let control_Print;

  //     control_Print = document.getElementById('__printingFrame');

  //     let doc = control_Print.contentWindow.document;
  //     doc.open();
  //     doc.write("<div style='color:red;'>I WANT TO PRINT THIS, NOT THE CURRENT HTML</div>");
  //     doc.close();

  //     control_Print = control_Print.contentWindow;
  //     control_Print.focus();
  //     control_Print.print();
  //  }

  // print4(){
  //   window.print();
  // }

  //  print2(printSectionId: string){savepdf1
  //   let popupWinindow
  //   let innerContents = document.getElementById(printSectionId).innerHTML;
  //   popupWinindow = window.open('', '', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
  //   popupWinindow.document.open();
  //   popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
  //   popupWinindow.document.close();
  // }

  back() {
    this.router.navigate(['/client/booking']);
  }


  ////------------------------++++
  //  pront(divName) {
  //   var printContents = document.getElementById(divName).innerHTML;
  //   var originalContents = document.body.innerHTML;        
  //   document.body.innerHTML = printContents;
  //   window.print();
  //   //document.body.innerHTML = originalContents;
  // }

  savepdf1(input) {
    console.log("saving pdf");
    var pdf = new jspdf('p', 'mm', '[250,185]');
    pdf.addHTML($(""), 15, 15, {}, function () {
      pdf.save(input + '.pdf');
      console.log("saved pdf");
      //this.back();
    });
  }
  name = 'Angular';
  print() {
    window.print();
  }

  public savepdf(input) {
    const data = document.getElementById('printSectionId');
    html2canvas(data).then(canvas => {
      const contentDataURL = canvas.toDataURL('image/png');
      if(canvas.height > 1250){
        window.print();
      }else{
        const fs = this._fsService.fs as any;
        var imageData = contentDataURL.replace(/^data:image\/\w+;base64,/, '');
        var path = '/tmp/' + input + '.png';
        fs.writeFileSync(path, imageData, { encoding: "base64" });
        this.printpdf(path);
      }
      
      // var contentWidth = canvas.width;
      // var contentHeight = canvas.height;
      // //One page pdf shows the canvas height generated by html pages.
      // var pageHeight = 800;
      // //html page height without pdf generation
      // var leftHeight = contentHeight;
      // //Page offset
      // var position = 0;
      // //a4 paper size [595.28, 841.89], html page generated canvas in pdf picture width
      // var imgWidth = 100;
      // var imgHeight = 230;
      // var pageData = canvas.toDataURL('image/jpeg', 1.0);
      // var pdf = new jsPDF('', 'mm', '[250,180]');
      // //pdf.setFontSize(22);
      // // pdf.text(20, 20, 'Example');
      // // pdf.autoPrint();
      // // window.open(pdf.output('bloburl'), '_blank');
      //const position = 0;

      // if (leftHeight < pageHeight) {  
      //  pdf.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHeight );
      // } else {
      //       while(leftHeight > 0) {
      //           pdf.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight)
      //           leftHeight -= pageHeight;
      //           position -= 841.89;
      //           //Avoid adding blank pages
      //           if(leftHeight > 0) {
      //             pdf.addPage();
      //             }   
      //         }
      //   }
      //pdf.save('content.pdf');
      //var data = pdf.output();
      
      // fs.writeFileSync(path, data, 'binary');
      
      // console.log(canvas.height);
      // if (canvas.height > 950) {
      //   window.print();
      // }
      // else //this.printpdf(path);
      //   window.print();
      this.back();
    });
  }
  printpdf(path) {
    console.log('lp -d BTP-S81-U-1 ' + path);

    this.childProcess.childProcess.exec('lp -d boslprinter ' + path, [], (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`);
        return;
      }
      console.log(`stdout: ${stdout}`);
      console.error(`stderr: ${stderr}`);
    });
  }

  public captureScreen(input) {
    setTimeout(() => {
      console.log('Test');
      try { this.savepdf(input); } catch (e) { console.log(e); }
    }, 2000 / 60);
  }
  qtyDetails: any = [];
  qtyno() {
    for (let i = 0; i < 2; i++) {
      this.qtyDetails.push({
        "qty": "Qty",
        "No": "No"
      })
    }
  }
  elementType = 'svg';
  value = 'someValue12340987';
  format = 'CODE128';
  lineColor = '#000000';
  width = 2;
  height = 100;
  displayValue = true;
  fontOptions = '';
  font = 'monospace';
  textAlign = 'center';
  textPosition = 'bottom';
  textMargin = 2;
  fontSize = 20;
  background = '#ffffff';
  margin = 20;
  marginTop = 10;
  marginBottom = 10;
  marginLeft = 15;
  marginRight = 10;
}
