import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'twoDecimalFormatNumber'
})
export class TwoDecimalFormatNumberPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value >= 0) {
      return parseFloat(value.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
    }
  }

}
