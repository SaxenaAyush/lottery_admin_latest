import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { DashboardComponent } from './components/distributor/dashboard/dashboard.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { LoginComponent } from './components/login/login.component';
import { ViewClientComponent } from './components/distributor/view-client/view-client.component';
import { ViewClientDetailsComponent } from './components/distributor/view-client-details/view-client-details.component';
import { AddNewClientComponent } from './components/distributor/add-new-client/add-new-client.component';
import { AuthGuard } from './components/guard/auth.guard';
import { AdminProfileComponent } from './components/distributor/admin-profile/admin-profile.component';
import { ViewDeactivatedclientComponent } from './components/distributor/view-Deactivatedclient/view-Deactivatedclient.component';
import { AboutUsComponent } from './components/aboutUs/aboutUs.component';
import { CertificateComponent } from './components/certificate/certificate.component';
import { TicketpreviewComponent } from './ticketpreview/ticketpreview.component';
import { ReprintticketComponent } from './reprintticket/reprintticket.component';
import { PrintresultComponent } from './printresult/printresult.component';
import { ReportPrintComponent } from './report-print/report-print.component';
import { CalimReprintticketComponent } from './claim-reprint/claim-reprintticket.component';
import { DistributorReportComponent } from './components/distributor/distributor-report/distributor-report.component';
import { ViewTransactionDetailsComponent } from './components/distributor/view-transaction-details/view-transaction-details.component';
import { SlotsReportsComponent } from './components/distributor/slots-reports/slots-reports.component';
import { WalletHistoryComponent } from './components/distributor/wallet-history/wallet-history.component';
import { DistributorDailyReportComponent } from './components/distributor/daily-report/distributor-daily-report.component';
import { DistributorSelfReportComponent } from './components/distributor/distributor-self-report/distributor-self-report.component';
import { RetailerWalletDetailsComponent } from './components/distributor/retailer-wallet-details/retailer-wallet-details.component';
import { PrintClaimVsBookingComponent } from './print-claim-vs-booking/print-claim-vs-booking.component';
import { FirstClaimBookingPrintComponent } from './first-claim-booking-print/first-claim-booking-print.component';
import { DurationReportPrintComponent } from './duration-report-print/duration-report-print.component';
import { AdminWalletComponent } from './components/distributor/admin-wallet/admin-wallet.component';
import { ResultComponent } from './components/distributor/result/result.component';
import { UserProfileComponent } from './components/distributor/user-profile/user-profile.component';
import { SettingComponent } from './components/distributor/setting/setting.component';
import { ChangeParentComponent } from './components/distributor/change-parent/change-parent.component';
import { AdvertisementComponent } from './components/distributor/advertisement/advertisement.component';
import { OutscreenResultComponent } from './components/distributor/outscreen-result/outscreen-result.component';
const routes: Routes = [

  {
    path: 'distributor',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'add-new-client',
        component: AddNewClientComponent
      },

      {
        path: 'view-client',
        component: ViewClientComponent
      },
      {
        path: 'view-client-details',
        component: ViewClientDetailsComponent
      },
      {
        path: 'view-Deactivatedclient-details',
        component: ViewDeactivatedclientComponent
      },

      {
        path: 'admin-profile',
        component: AdminProfileComponent
      },
      {
        path: 'distributor-report',
        component: DistributorReportComponent
      },
      {
        path: 'view-Transaction-report',
        component: ViewTransactionDetailsComponent
      },
      {
        path: 'slot-report',
        component: SlotsReportsComponent
      },
      {
        path: 'wallet-History',
        component: WalletHistoryComponent
      },
      {
        path: 'distributor-daily-report',
        component: DistributorDailyReportComponent
      },
      {
        path: 'distributor-self-report',
        component: DistributorSelfReportComponent
      },
      {
        path: 'retailer-wallet-report',
        component: RetailerWalletDetailsComponent
      },
      {
        path: 'admin-wallet',
        component: AdminWalletComponent
      },
      {
        path: 'result',
        component: ResultComponent
      },
      {
        path: 'user-profile',
        component: UserProfileComponent
      },
      {
        path: 'setting',
        component: SettingComponent
      },        
      {
        path: 'change-parent',
        component: ChangeParentComponent
      },
      {
        path: 'advertise',
        component: AdvertisementComponent
      },
      {
        path: 'outside',
        component: OutscreenResultComponent
      },
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: 'login',
        component: LoginComponent
      },


    ]
  },
 

{
  path: 'aboutUs',
  component: AboutUsComponent
  },
  {
    path: 'certificate',
    component: CertificateComponent
    },
    {
      path: 'ticket-preview',
      component: TicketpreviewComponent,
      },
      {
        path: 'reprint-ticket',
        component: ReprintticketComponent,
        },
        {
          path: 'report-print',
          component: ReportPrintComponent
        },
        {
          path: 'result-print',
          component: PrintresultComponent
        },
        {
          path: 'claim-reprint',
          component: CalimReprintticketComponent
        },
        {
          path: 'claim-vs-booking-print',
          component: PrintClaimVsBookingComponent
        },
        {
          path: 'first-claim-vs-booking-print',
          component: FirstClaimBookingPrintComponent
        },
        {
          path: 'duration-report-print',
          component: DurationReportPrintComponent
        },
  { path: '**', redirectTo: '', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}