import { TestBed } from '@angular/core/testing';

import { LotteryStorageService } from './lottery-storage.service';

describe('LotteryStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LotteryStorageService = TestBed.get(LotteryStorageService);
    expect(service).toBeTruthy();
  });
});
