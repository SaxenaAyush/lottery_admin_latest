import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {



  private loggedInUserDetails = new BehaviorSubject<any>({});
  user = this.loggedInUserDetails.asObservable();



  constructor() {
  }
  updateUserDetails(user) {
    this.loggedInUserDetails.next(user);
  }
  
}
