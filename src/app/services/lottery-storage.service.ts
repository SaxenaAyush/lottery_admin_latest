import { Injectable } from '@angular/core';
declare function escape(s: string): string;
declare function unescape(s: string): string;
@Injectable({
  providedIn: 'root'
})
export class LotteryStorageService {

  constructor() {
    if (!this.get('currentUser')) {
      localStorage.setItem('lottery', '');
    }
  }

  get(key?: string) {
    const val = JSON.parse(decodeURIComponent(escape(localStorage.getItem('lottery') ? atob(localStorage.getItem('lottery')) : '{}')));
    return val[key];
  }

  set(key: string, val: any) {
    const all = JSON.parse(decodeURIComponent(escape(localStorage.getItem('lottery') ? atob(localStorage.getItem('lottery')) : '{}')));
    all[key] = val;
    return localStorage.setItem('lottery', btoa(unescape(encodeURIComponent(JSON.stringify(all)))));
  }

}
