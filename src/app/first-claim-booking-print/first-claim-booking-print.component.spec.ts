import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstClaimBookingPrintComponent } from './first-claim-booking-print.component';

describe('FirstClaimBookingPrintComponent', () => {
  let component: FirstClaimBookingPrintComponent;
  let fixture: ComponentFixture<FirstClaimBookingPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstClaimBookingPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstClaimBookingPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
