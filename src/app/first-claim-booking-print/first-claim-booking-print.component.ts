import { Component, OnInit } from '@angular/core';
import { LotteryStorageService } from '../services/lottery-storage.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { LotteryHttpService } from '../services/lottery-http.service';
import { SweetAlertService } from '../common/sharaed/sweetalert2.service';

@Component({
  selector: 'app-first-claim-booking-print',
  templateUrl: './first-claim-booking-print.component.html',
  styleUrls: ['./first-claim-booking-print.component.scss']
})
export class FirstClaimBookingPrintComponent implements OnInit {
  dailyReportFiltered:any[]=[];
  slotTime:any;
  Date:any;
  TodayDate:any;
  viewSlotsDetails = this.storageService.get('claimvsbookingReport');
  viewSlotsId = this.storageService.get('id');
  date = this.storageService.get('date');

  viewSlotsdrawTime = this.storageService.get('drawTime');
  constructor( private storageService: LotteryStorageService,private route: Router,
     private alertService: SweetAlertService,private datePipe: DatePipe,
     private lotteryService: LotteryHttpService) { }

  ngOnInit() {
    this.dailyReportFiltered = this.viewSlotsDetails
    console.log('National',this.dailyReportFiltered);
    this.slotTime = this.viewSlotsdrawTime
    this.Date =     this.date

    setTimeout(() => {
      console.log('Test');
      window.print();
      this.Back();
  }, 1000/60);  
  }
  Back(){
    this.route.navigate(['/client/client-vs-booking']) 
    
  }
}
