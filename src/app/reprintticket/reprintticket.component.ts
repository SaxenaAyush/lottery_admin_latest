import { Component, OnInit } from '@angular/core';
import getMAC, { isMAC } from 'getmac'
import {ActivatedRoute, Router} from '@angular/router';
import {LotteryStorageService} from '../services/lottery-storage.service';
import {ViewChild} from '@angular/core';
import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas';
import * as jsPDF from 'jspdf'; 
import * as $ from '../../../node_modules/jquery';
import {  ElementRef } from '@angular/core';

@Component({
  selector: 'app-reprintticket',
  templateUrl: './reprintticket.component.html',
  styleUrls: ['./reprintticket.component.scss']
})
export class ReprintticketComponent implements OnInit {

  constructor( private router: Router, private storageService: LotteryStorageService) {
    
  }
  singlePrice=this.storageService.get('singleprice');
  userD = this.storageService.get('currentUser');
  retailerID = this.userD.user.userCode;
  billModel : any = [];
  billModel1 : any = [];
  slots : any = [];
  totalModel : any = {
    "total": 0
  }
  today : any;
  ngOnInit() {
    this.qtyno();
    console.log(getMAC());
    setInterval(() => {
      this.today = Date.now()
    }, 1000);
    this.billModel1=  this.storageService.get('reprint');
    console.log('bill model',this.billModel1);
    this.billModel=this.billModel1.content;
    this.sepratedata();
  }
  bill = {
  
    "ticketid" : 324,
     "values " : {
       "slots" : "slots",
       "bookingDetails" : "bookingDetail",
       "ticketNumbers" : "ticketNumbers"
     }
  }
  totaltickets : any = [];
  ticketcount : any = [];
  sepratedata(){
    for(let bill of this.billModel){
    
        this.ticketcount.push({
          "id" : bill.bookingDetails.id,
          "ticketId" : bill.bookingDetails.ticketId
        });
  
  this.totaltickets.push({
    "id" : bill.bookingDetails.id,
    "retailerId": bill.bookingDetails.retailerId,
    "bookingDate" : bill.bookingDetails.bookingDate,
    "createDate" : bill.bookingDetails.createDate,
    "totalPrice" : bill.bookingDetails.totalPrice,
     "slot" :bill.bookingDetails.slot.drawTimeAm,
     "totalTicket" : bill.bookingDetails.totalTicket,
     "ticketNumbers" : bill.ticketNumbers,
     "ticketId" : parseInt(bill.bookingDetails.ticketId)
  });
    //   }
    // }
  
    }
    console.log('totaltickets',this.totaltickets);
    console.log('totaltickets',this.totaltickets);
    // setTimeout(function() {  this.captureScreen(); }, 5000);
     this.captureScreen();
  }
  public captureScreen() {
    setTimeout(() => {
      console.log('Test');
      window.print();
      this.back();
  }, 1000/60);  
    }
    qtyDetails : any = [];
    qtyno(){
      for(let i=0; i<3; i++)
      {
  this.qtyDetails.push({
    "qty" : "Qty",
    "No" : "No"
  })
      }
    }
    back(){
      this.router.navigate(['/client/ticket-history']);
    }
}
