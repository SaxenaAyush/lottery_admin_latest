import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReprintticketComponent } from './reprintticket.component';

describe('ReprintticketComponent', () => {
  let component: ReprintticketComponent;
  let fixture: ComponentFixture<ReprintticketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReprintticketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReprintticketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
