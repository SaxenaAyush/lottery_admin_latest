import { Component, OnInit } from '@angular/core';
import { LotteryStorageService } from '../services/lottery-storage.service';
import { Router } from '@angular/router';
import { SweetAlertService } from '../common/sharaed/sweetalert2.service';
import { DatePipe } from '@angular/common';
import { LotteryHttpService } from '../services/lottery-http.service';

@Component({
  selector: 'app-print-claim-vs-booking',
  templateUrl: './print-claim-vs-booking.component.html',
  styleUrls: ['./print-claim-vs-booking.component.scss']
})
export class PrintClaimVsBookingComponent implements OnInit {
  SlotsDetails:any[]=[];
  slotTime:any;
  Date:any;
  TodayDate:any;
  viewSlotsDetails = this.storageService.get('slotsData');
  viewSlotsId = this.storageService.get('id');
  date = this.storageService.get('date');

  viewSlotsdrawTime = this.storageService.get('drawTime');
  constructor( private storageService: LotteryStorageService,private route: Router, private alertService: SweetAlertService,
    private datePipe: DatePipe,private lotteryService: LotteryHttpService) { }

  ngOnInit() {
    this.SlotsDetails = this.viewSlotsDetails.bookingDetail
    console.log('National',this.SlotsDetails);
    this.slotTime = this.viewSlotsdrawTime
    this.Date =     this.date

    setTimeout(() => {
      console.log('Test');
      window.print();
      this.Back();
  }, 1000/60);  
  }
  Back(){
    this.route.navigate(['/client/client-vs-booking']) 

  }

}
