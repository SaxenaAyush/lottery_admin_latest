import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintClaimVsBookingComponent } from './print-claim-vs-booking.component';

describe('PrintClaimVsBookingComponent', () => {
  let component: PrintClaimVsBookingComponent;
  let fixture: ComponentFixture<PrintClaimVsBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintClaimVsBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintClaimVsBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
