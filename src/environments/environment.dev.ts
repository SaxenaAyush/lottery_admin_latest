// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `index.ts`, but if you do
// `ng build --env=prod` then `index.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  baseUrl: 'http://lotterydev.us-east-2.elasticbeanstalk.com',

    //baseUrl: 'http://env-0661550.mj.milesweb.cloud',//development
 //baseUrl: 'http://env-3353070.mj.milesweb.cloud',
};
